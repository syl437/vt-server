<?php
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\CompanySubcategoryTranslation
 *
 * @property int $id
 * @property int $company_subcategory_id
 * @property string $title
 * @property string $locale
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanySubcategoryTranslation whereCompanySubcategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanySubcategoryTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanySubcategoryTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanySubcategoryTranslation whereTitle($value)
 */
	class CompanySubcategoryTranslation extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Code
 *
 * @property int $id
 * @property int|null $rewardable_id
 * @property string|null $rewardable_type
 * @property string $code
 * @property float|null $reward
 * @property int $absolute
 * @property string|null $data
 * @property int $is_used
 * @property string|null $expiration
 * @property string|null $label
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Code whereAbsolute($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Code whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Code whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Code whereExpiration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Code whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Code whereIsUsed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Code whereLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Code whereReward($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Code whereRewardableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Code whereRewardableType($value)
 */
	class Code extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Region
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Plank\Mediable\MediableCollection|\App\Models\Company[] $companies
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RegionTranslation[] $translations
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region orWhereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region orWhereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region translated()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Region withTranslation()
 */
	class Region extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\CouponTranslation
 *
 * @property int $id
 * @property int $coupon_id
 * @property string $title
 * @property string|null $description
 * @property string $locale
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CouponTranslation whereCouponId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CouponTranslation whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CouponTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CouponTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CouponTranslation whereTitle($value)
 */
	class CouponTranslation extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Banner
 *
 * @property int $id
 * @property string $type
 * @property int|null $company_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read mixed $company
 * @property-read mixed $image
 * @property-read \Illuminate\Database\Eloquent\Collection|\Plank\Mediable\Media[] $media
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Banner whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Banner whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Banner whereHasMedia($tags, $match_all = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Banner whereHasMediaMatchAll($tags)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Banner whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Banner whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Banner whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Banner withMedia($tags = array(), $match_all = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Banner withMediaMatchAll($tags = array())
 */
	class Banner extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\File
 *
 * @property int $id
 * @property string $disk
 * @property string $directory
 * @property string $filename
 * @property string $extension
 * @property string $mime_type
 * @property string $aggregate_type
 * @property int $size
 * @property int $compressed
 * @property string $status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Comment[] $comments
 * @property-read \Illuminate\Database\Eloquent\Collection|\Plank\Mediable\Media[] $media
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereAggregateType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereCompressed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereDirectory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereDisk($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereExtension($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereFilename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereHasMedia($tags, $match_all = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereHasMediaMatchAll($tags)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereMimeType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File withMedia($tags = array(), $match_all = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File withMediaMatchAll($tags = array())
 */
	class File extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\CompanyCategoryTranslation
 *
 * @property int $id
 * @property int $company_category_id
 * @property string $title
 * @property string $locale
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyCategoryTranslation whereCompanyCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyCategoryTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyCategoryTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyCategoryTranslation whereTitle($value)
 */
	class CompanyCategoryTranslation extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\CompanySubcategory
 *
 * @property int $id
 * @property int $company_category_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Plank\Mediable\MediableCollection|\App\Models\Company[] $companies
 * @property-read \App\Models\CompanyCategory $company_category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CompanySubcategoryTranslation[] $translations
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanySubcategory listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanySubcategory notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanySubcategory orWhereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanySubcategory orWhereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanySubcategory translated()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanySubcategory translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanySubcategory whereCompanyCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanySubcategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanySubcategory whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanySubcategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanySubcategory whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanySubcategory whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanySubcategory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanySubcategory withTranslation()
 */
	class CompanySubcategory extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $phone
 * @property string $country
 * @property int $points
 * @property string|null $api_token
 * @property string|null $remember_token
 * @property string|null $push_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Comment[] $comments
 * @property-read \Plank\Mediable\MediableCollection|\App\Models\Coupon[] $coupons
 * @property-read \Illuminate\Database\Eloquent\Collection|\Skybluesofa\Followers\Models\Follower[] $followers
 * @property-read \Illuminate\Database\Eloquent\Collection|\Skybluesofa\Followers\Models\Follower[] $following
 * @property-read mixed $avatar
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Lead[] $leads
 * @property-read \Illuminate\Database\Eloquent\Collection|\Plank\Mediable\Media[] $media
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Aira\Promocodes\Model\Promocode[] $promocodes
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Rate[] $rates
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereApiToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereHasMedia($tags, $match_all = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereHasMediaMatchAll($tags)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePushId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User withMedia($tags = array(), $match_all = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User withMediaMatchAll($tags = array())
 */
	class User extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Point
 *
 * @property int $id
 * @property int $quantity
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Point whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Point whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Point whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Point whereUpdatedAt($value)
 */
	class Point extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\FaqTranslation
 *
 * @property int $id
 * @property int $faq_id
 * @property string $question
 * @property string $answer
 * @property string $locale
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FaqTranslation whereAnswer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FaqTranslation whereFaqId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FaqTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FaqTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FaqTranslation whereQuestion($value)
 */
	class FaqTranslation extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Company
 *
 * @property int $id
 * @property int $company_subcategory_id
 * @property int $region_id
 * @property string $address
 * @property string $country
 * @property string|null $lat
 * @property string|null $lng
 * @property string|null $website
 * @property string|null $phone
 * @property int $from_user
 * @property int $approved
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\CompanySubcategory $company_subcategory
 * @property-read \Plank\Mediable\MediableCollection|\App\Models\Coupon[] $coupons
 * @property-read mixed $company_category_id
 * @property-read mixed $logo
 * @property-read mixed $main_video
 * @property-read mixed $pin
 * @property-read mixed $rate
 * @property-read mixed $stars
 * @property-read mixed $thumbnail
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Lead[] $leads
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Manager[] $managers
 * @property-read \Illuminate\Database\Eloquent\Collection|\Plank\Mediable\Media[] $media
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Rate[] $rates
 * @property-read \App\Models\Region $region
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CompanyTranslation[] $translations
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company orWhereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company orWhereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company translated()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereApproved($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereCompanySubcategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereFromUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereHasMedia($tags, $match_all = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereHasMediaMatchAll($tags)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereLng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereRegionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereWebsite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company withMedia($tags = array(), $match_all = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company withMediaMatchAll($tags = array())
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company withTranslation()
 */
	class Company extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Follower
 *
 * @property int $id
 * @property int $sender_id
 * @property string $sender_type
 * @property int $recipient_id
 * @property string $recipient_type
 * @property int $status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Follower whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Follower whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Follower whereRecipientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Follower whereRecipientType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Follower whereSenderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Follower whereSenderType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Follower whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Follower whereUpdatedAt($value)
 */
	class Follower extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\CompanyTranslation
 *
 * @property int $id
 * @property int $company_id
 * @property string $title
 * @property string $description
 * @property string $locale
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyTranslation whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyTranslation whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyTranslation whereTitle($value)
 */
	class CompanyTranslation extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\CompanyCategory
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Plank\Mediable\MediableCollection|\App\Models\Company[] $companies
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CompanySubcategory[] $company_subcategories
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CompanyCategoryTranslation[] $translations
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyCategory listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyCategory notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyCategory orWhereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyCategory orWhereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyCategory translated()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyCategory translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyCategory whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyCategory whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyCategory whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyCategory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompanyCategory withTranslation()
 */
	class CompanyCategory extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Rate
 *
 * @property int $id
 * @property int $user_id
 * @property int $company_id
 * @property int $mark
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Company $company
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rate whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rate whereMark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rate whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rate whereUserId($value)
 */
	class Rate extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Manager
 *
 * @property int $id
 * @property string|null $name
 * @property string $email
 * @property string $password
 * @property int $company_id
 * @property int $owner
 * @property string|null $api_token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Company $company
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Manager whereApiToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Manager whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Manager whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Manager whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Manager whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Manager whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Manager whereOwner($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Manager wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Manager whereUpdatedAt($value)
 */
	class Manager extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Comment
 *
 * @property int $id
 * @property int $user_id
 * @property int $media_id
 * @property string $content
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @property-read \App\Models\File $media
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comment whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comment whereMediaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comment whereUserId($value)
 */
	class Comment extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Faq
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\FaqTranslation[] $translations
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq orWhereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq orWhereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq translated()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq withTranslation()
 */
	class Faq extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Lead
 *
 * @property int $id
 * @property int $user_id
 * @property int $company_id
 * @property int $quantity
 * @property string $start
 * @property string|null $end
 * @property string|null $comments
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Company $company
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lead whereComments($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lead whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lead whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lead whereEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lead whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lead whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lead whereStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lead whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lead whereUserId($value)
 */
	class Lead extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Admin
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $phone
 * @property string|null $remember_token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereUsername($value)
 */
	class Admin extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\RegionTranslation
 *
 * @property int $id
 * @property int $region_id
 * @property string $title
 * @property string $locale
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegionTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegionTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegionTranslation whereRegionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegionTranslation whereTitle($value)
 */
	class RegionTranslation extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Coupon
 *
 * @property int $id
 * @property int $company_id
 * @property int $price
 * @property int $quantity
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Company $company
 * @property-read mixed $image
 * @property-read mixed $qr
 * @property-read \Illuminate\Database\Eloquent\Collection|\Plank\Mediable\Media[] $media
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CouponTranslation[] $translations
 * @property-read \Plank\Mediable\MediableCollection|\App\Models\User[] $users
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Coupon listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Coupon notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Coupon orWhereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Coupon orWhereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Coupon translated()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Coupon translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Coupon whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Coupon whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Coupon whereHasMedia($tags, $match_all = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Coupon whereHasMediaMatchAll($tags)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Coupon whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Coupon wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Coupon whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Coupon whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Coupon whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Coupon whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Coupon withMedia($tags = array(), $match_all = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Coupon withMediaMatchAll($tags = array())
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Coupon withTranslation()
 */
	class Coupon extends \Eloquent {}
}

