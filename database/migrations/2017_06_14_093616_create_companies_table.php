<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_subcategory_id')->unsigned();
            $table->foreign('company_subcategory_id')->references('id')->on('company_subcategories')->onDelete('cascade');
            $table->integer('region_id')->unsigned();
            $table->foreign('region_id')->references('id')->on('regions')->onDelete('cascade');
            $table->string('address');
            $table->string('country');
            $table->string('lat')->nullable();
            $table->string('lng')->nullable();
            $table->string('website')->nullable();
            $table->string('phone')->nullable();
            $table->tinyInteger('from_user')->default(0);
            $table->boolean('approved')->default(0);
            $table->timestamps();
        });

        Schema::create('company_translations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->string('title');
            $table->text('description');
            $table->string('locale')->index();
            $table->unique(['company_id','locale']);
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_translations');
        Schema::dropIfExists('companies');
    }
}
