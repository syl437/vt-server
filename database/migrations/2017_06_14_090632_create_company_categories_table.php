<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('company_category_translations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('company_category_id')->unsigned();
            $table->string('title');
            $table->string('locale')->index();
            $table->unique(['company_category_id','locale']);
            $table->foreign('company_category_id')->references('id')->on('company_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_category_translations');
        Schema::dropIfExists('company_categories');
    }
}
