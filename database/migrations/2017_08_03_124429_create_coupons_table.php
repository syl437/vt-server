<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->integer('price')->unsigned();
            $table->integer('quantity');
            $table->timestamps();
        });

        Schema::create('coupon_translations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('coupon_id')->unsigned();
            $table->string('title');
            $table->text('description')->nullable();
            $table->string('locale')->index();
            $table->unique(['coupon_id','locale']);
            $table->foreign('coupon_id')->references('id')->on('coupons')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupon_translations');
        Schema::dropIfExists('coupons');
    }
}
