<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanySubcategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_subcategories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_category_id')->unsigned();
            $table->foreign('company_category_id')->references('id')->on('company_categories')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('company_subcategory_translations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('company_subcategory_id')->unsigned();
            $table->string('title');
            $table->string('locale')->index();
            $table->unique(['company_subcategory_id','locale'], 'csil');
            $table->foreign('company_subcategory_id')->references('id')->on('company_subcategories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_subcategory_translations');
        Schema::dropIfExists('company_subcategories');
    }
}
