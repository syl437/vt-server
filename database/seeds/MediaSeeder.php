<?php

use Illuminate\Database\Seeder;

class MediaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

//        $media1 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_main_pictures/hotels.jpg");
//        $media2 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_main_pictures/restaurants.jpg");
//        $media3 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_main_pictures/attractions.jpg");
//        $media4 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_main_pictures/parties.jpg");
//
//        $cat1 = \App\Models\CompanyCategory::find(1);
//        $cat2 = \App\Models\CompanyCategory::find(2);
//        $cat3 = \App\Models\CompanyCategory::find(3);
//        $cat4 = \App\Models\CompanyCategory::find(4);
//
//        $cat1->attachMedia($media1, 'image');
//        $cat2->attachMedia($media2, 'image');
//        $cat3->attachMedia($media3, 'image');
//        $cat4->attachMedia($media4, 'image');
//
//        $media1->status = 'approved';
//        $media1->save();
//        $media2->status = 'approved';
//        $media2->save();
//        $media3->status = 'approved';
//        $media3->save();
//        $media4->status = 'approved';
//        $media4->save();

        $media1 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_banners/banner.jpg");
        $media2 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_banners/banner1.png");
        $media3 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_banners/banner2.png");
        $media4 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_banners/banner3.png");

        $cat1 = \App\Models\Banner::find(1);
        $cat2 = \App\Models\Banner::find(2);
        $cat3 = \App\Models\Banner::find(3);
        $cat4 = \App\Models\Banner::find(4);

        $cat1->attachMedia($media1, 'image');
        $cat2->attachMedia($media2, 'image');
        $cat3->attachMedia($media3, 'image');
        $cat4->attachMedia($media4, 'image');

        $media1->status = 'approved';
        $media1->save();
        $media2->status = 'approved';
        $media2->save();
        $media3->status = 'approved';
        $media3->save();
        $media4->status = 'approved';
        $media4->save();

        $usermedia1 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_avatars/gofman.jpg");
        $usermedia2 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_avatars/bradbury.jpg");
        $usermedia3 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_avatars/gaiman.jpg");
        $usermedia4 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_avatars/king.jpg");
        $usermedia5 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_avatars/martin.jpg");
        $usermedia6 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_avatars/munro.jpg");
        $usermedia7 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_avatars/murakami.jpg");
        $usermedia8 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_avatars/palahniuk.jpg");
        $usermedia9 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_avatars/rowling.jpg");
        $usermedia10 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_avatars/tartt.jpg");

        $user1 = \App\Models\User::find(1); // Me
        $user2 = \App\Models\User::find(2); // Bradbary
        $user3 = \App\Models\User::find(3); // Gaiman
        $user4 = \App\Models\User::find(4); // King
        $user5 = \App\Models\User::find(5); // Martin
        $user6 = \App\Models\User::find(6); // Munro
        $user7 = \App\Models\User::find(7); // Murakami
        $user8 = \App\Models\User::find(8); // Palahniuk
        $user9 = \App\Models\User::find(9); // Rowling
        $user10 = \App\Models\User::find(10); // Tartt

        $user1->attachMedia($usermedia1, 'avatar');
        $user2->attachMedia($usermedia2, 'avatar');
        $user3->attachMedia($usermedia3, 'avatar');
        $user4->attachMedia($usermedia4, 'avatar');
        $user5->attachMedia($usermedia5, 'avatar');
        $user6->attachMedia($usermedia6, 'avatar');
        $user7->attachMedia($usermedia7, 'avatar');
        $user8->attachMedia($usermedia8, 'avatar');
        $user9->attachMedia($usermedia9, 'avatar');
        $user10->attachMedia($usermedia10, 'avatar');

        $usermedia1->status = 'approved';
        $usermedia1->save();
        $usermedia2->status = 'approved';
        $usermedia2->save();
        $usermedia3->status = 'approved';
        $usermedia3->save();
        $usermedia4->status = 'approved';
        $usermedia4->save();
        $usermedia5->status = 'approved';
        $usermedia5->save();
        $usermedia6->status = 'approved';
        $usermedia6->save();
        $usermedia7->status = 'approved';
        $usermedia7->save();
        $usermedia8->status = 'approved';
        $usermedia8->save();
        $usermedia9->status = 'approved';
        $usermedia9->save();
        $usermedia10->status = 'approved';
        $usermedia10->save();

        $logo1 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_logos/aroma.gif");
        $logo2 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_logos/cafe.png");
        $logo3 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_logos/carlton.png");
        $logo4 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_logos/dominos.jpg");
        $logo5 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_logos/japanika.jpg");
        $logo6 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_logos/me.png");
        $logo7 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_logos/molly.png");
        $logo8 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_logos/saloon.png");
        $logo9 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_logos/sheraton.gif");
        $logo10 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_logos/tapper.png");

        $company1 = \App\Models\Company::find(1); // Aroma
        $company2 = \App\Models\Company::find(2); // Cafe Cafe
        $company3 = \App\Models\Company::find(3); // Carlton
        $company4 = \App\Models\Company::find(4); // Dominos
        $company5 = \App\Models\Company::find(5); // Japanika
        $company6 = \App\Models\Company::find(6); // Museum
        $company7 = \App\Models\Company::find(7); // Molly Blooms
        $company8 = \App\Models\Company::find(8); // Burger Saloon
        $company9 = \App\Models\Company::find(9); // Sheraton
        $company10 = \App\Models\Company::find(10); // Tapper

        $company1->attachMedia($logo1, 'logo');
        $company2->attachMedia($logo2, 'logo');
        $company3->attachMedia($logo3, 'logo');
        $company4->attachMedia($logo4, 'logo');
        $company5->attachMedia($logo5, 'logo');
        $company6->attachMedia($logo6, 'logo');
        $company7->attachMedia($logo7, 'logo');
        $company8->attachMedia($logo8, 'logo');
        $company9->attachMedia($logo9, 'logo');
        $company10->attachMedia($logo10, 'logo');

        $logo1->status = 'approved';
        $logo1->save();
        $logo2->status = 'approved';
        $logo2->save();
        $logo3->status = 'approved';
        $logo3->save();
        $logo4->status = 'approved';
        $logo4->save();
        $logo5->status = 'approved';
        $logo5->save();
        $logo6->status = 'approved';
        $logo6->save();
        $logo7->status = 'approved';
        $logo7->save();
        $logo8->status = 'approved';
        $logo8->save();
        $logo9->status = 'approved';
        $logo9->save();
        $logo10->status = 'approved';
        $logo10->save();

        dispatch(new \App\Jobs\CreateLogoPin($logo1));
        dispatch(new \App\Jobs\CreateLogoPin($logo2));
        dispatch(new \App\Jobs\CreateLogoPin($logo3));
        dispatch(new \App\Jobs\CreateLogoPin($logo4));
        dispatch(new \App\Jobs\CreateLogoPin($logo5));
        dispatch(new \App\Jobs\CreateLogoPin($logo6));
        dispatch(new \App\Jobs\CreateLogoPin($logo7));
        dispatch(new \App\Jobs\CreateLogoPin($logo8));
        dispatch(new \App\Jobs\CreateLogoPin($logo9));
        dispatch(new \App\Jobs\CreateLogoPin($logo10));

        $video1 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "original/aroma.mp4");
        $video2 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "original/cafe.mp4");
        $video3 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "original/carlton.mp4");
        $video4 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "original/dominos.mp4");
        $video5 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "original/japanika.mp4");
        $video6 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "original/me.mp4");
        $video7 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "original/molly.mp4");
        $video8 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "original/saloon.mp4");
        $video9 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "original/sheraton.mp4");

        $video1->status = 'approved';
        $video1->save();
        $video2->status = 'approved';
        $video2->save();
        $video3->status = 'approved';
        $video3->save();
        $video4->status = 'approved';
        $video4->save();
        $video5->status = 'approved';
        $video5->save();
        $video6->status = 'approved';
        $video6->save();
        $video7->status = 'approved';
        $video7->save();
        $video8->status = 'approved';
        $video8->save();
        $video9->status = 'approved';
        $video9->save();

        $company1->attachMedia($video1, 'main_video');
        $company2->attachMedia($video2, 'main_video');
        $company3->attachMedia($video3, 'main_video');
        $company4->attachMedia($video4, 'main_video');
        $company5->attachMedia($video5, 'main_video');
        $company6->attachMedia($video6, 'main_video');
        $company7->attachMedia($video7, 'main_video');
        $company8->attachMedia($video8, 'main_video');
        $company9->attachMedia($video9, 'main_video');

        dispatch(new \App\Jobs\CompressVideo($video1, $company1, 'main_video', null));
        dispatch(new \App\Jobs\CompressVideo($video2, $company2, 'main_video', null));
        dispatch(new \App\Jobs\CompressVideo($video3, $company3, 'main_video', null));
        dispatch(new \App\Jobs\CompressVideo($video4, $company4, 'main_video', null));
        dispatch(new \App\Jobs\CompressVideo($video5, $company5, 'main_video', null));
        dispatch(new \App\Jobs\CompressVideo($video6, $company6, 'main_video', null));
        dispatch(new \App\Jobs\CompressVideo($video7, $company7, 'main_video', null));
        dispatch(new \App\Jobs\CompressVideo($video8, $company8, 'main_video', null));
        dispatch(new \App\Jobs\CompressVideo($video9, $company9, 'main_video', null));

        $couponimage1 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_coupons/1.jpg");
        $couponimage2 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_coupons/2.jpg");
        $couponimage3 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_coupons/3.jpg");
        $couponimage4 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_coupons/4.jpg");
        $couponimage5 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_coupons/5.jpg");
        $couponimage6 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_coupons/6.jpg");
        $couponimage7 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_coupons/7.jpg");
        $couponimage8 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_coupons/8.jpg");
        $couponimage9 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_coupons/9.jpg");
        $couponimage10 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_coupons/10.jpg");

        $coupon1 = \App\Models\Coupon::find(1);
        $coupon2 = \App\Models\Coupon::find(3);
        $coupon3 = \App\Models\Coupon::find(5);
        $coupon4 = \App\Models\Coupon::find(7);
        $coupon5 = \App\Models\Coupon::find(9);
        $coupon6 = \App\Models\Coupon::find(11);
        $coupon7 = \App\Models\Coupon::find(13);
        $coupon8 = \App\Models\Coupon::find(15);
        $coupon9 = \App\Models\Coupon::find(17);
        $coupon10 = \App\Models\Coupon::find(19);

        $coupon1->attachMedia($couponimage1, 'coupon');
        $coupon2->attachMedia($couponimage2, 'coupon');
        $coupon3->attachMedia($couponimage3, 'coupon');
        $coupon4->attachMedia($couponimage4, 'coupon');
        $coupon5->attachMedia($couponimage5, 'coupon');
        $coupon6->attachMedia($couponimage6, 'coupon');
        $coupon7->attachMedia($couponimage7, 'coupon');
        $coupon8->attachMedia($couponimage8, 'coupon');
        $coupon9->attachMedia($couponimage9, 'coupon');
        $coupon10->attachMedia($couponimage10, 'coupon');

        $couponimage1->status = 'approved';
        $couponimage1->save();
        $couponimage2->status = 'approved';
        $couponimage2->save();
        $couponimage3->status = 'approved';
        $couponimage3->save();
        $couponimage4->status = 'approved';
        $couponimage4->save();
        $couponimage5->status = 'approved';
        $couponimage5->save();
        $couponimage6->status = 'approved';
        $couponimage6->save();
        $couponimage7->status = 'approved';
        $couponimage7->save();
        $couponimage8->status = 'approved';
        $couponimage8->save();
        $couponimage9->status = 'approved';
        $couponimage9->save();
        $couponimage10->status = 'approved';
        $couponimage10->save();

        $review1 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "original/bar1.mp4");
        $review2 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "original/bar2.mp4");
        $review3 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "original/burger1.mp4");
        $review4 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "original/burger2.mp4");
        $review5 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "original/cafe1.mp4");
        $review6 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "original/cafe2.mp4");
        $review7 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "original/cafe3.mp4");
        $review8 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "original/cafe4.mp4");
        $review9 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "original/hotel1.mp4");
        $review10 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "original/hotel2.mp4");
        $review11 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "original/hotel3.mp4");
        $review12 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "original/museum1.mp4");
        $review13 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "original/museum2.mp4");
        $review14 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "original/pizza1.mp4");
        $review15 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "original/pizza2.mp4");
        $review16 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "original/sushi1.mp4");
        $review17 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "original/sushi2.mp4");
        $review18 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "original/sushi3.mp4");
        $review19 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "original/tapper1.mp4");
        $review20 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "original/tapper2.mp4");

        $review1->status = 'approved';
        $review1->save();
        $review2->status = 'approved';
        $review2->save();
        $review3->status = 'approved';
        $review3->save();
        $review4->status = 'approved';
        $review4->save();
        $review5->status = 'approved';
        $review5->save();
        $review6->status = 'approved';
        $review6->save();
        $review7->status = 'approved';
        $review7->save();
        $review8->status = 'approved';
        $review8->save();
        $review9->status = 'approved';
        $review9->save();
        $review10->status = 'approved';
        $review10->save();
        $review11->status = 'approved';
        $review11->save();
        $review12->status = 'approved';
        $review12->save();
        $review13->status = 'approved';
        $review13->save();
        $review14->status = 'approved';
        $review14->save();
        $review15->status = 'approved';
        $review15->save();
        $review16->status = 'approved';
        $review16->save();
        $review17->status = 'approved';
        $review17->save();
        $review18->status = 'approved';
        $review18->save();
        $review19->status = 'approved';
        $review19->save();
        $review20->status = 'approved';
        $review20->save();

        $user1->attachMedia($review1, 'אהבתי את הפאב');
        $company7->attachMedia($review1, 'אהבתי את הפאב');
        dispatch(new \App\Jobs\CompressVideo($review1, $company7, 'אהבתי את הפאב', $user1));

        $user4->attachMedia($review2, 'אווירה מדהימה!');
        $company7->attachMedia($review2, 'אווירה מדהימה!');
        dispatch(new \App\Jobs\CompressVideo($review2, $company7, 'אווירה מדהימה!', $user4));

        $user5->attachMedia($review3, 'בורגרים מצוינים');
        $company8->attachMedia($review3, 'בורגרים מצוינים');
        dispatch(new \App\Jobs\CompressVideo($review3, $company8, 'בורגרים מצוינים', $user5));

        $user3->attachMedia($review4, 'המקומ מה זה טוב');
        $company8->attachMedia($review4, 'המקומ מה זה טוב');
        dispatch(new \App\Jobs\CompressVideo($review4, $company8, 'המקומ מה זה טוב', $user3));

        $user6->attachMedia($review5, 'קפה פה נהדר');
        $company1->attachMedia($review5, 'קפה פה נהדר');
        dispatch(new \App\Jobs\CompressVideo($review5, $company1, 'קפה פה נהדר', $user6));

        $user9->attachMedia($review6, 'סנדויצ`ים מעוד טעימים');
        $company1->attachMedia($review6, 'סנדויצ`ים מעוד טעימים');
        dispatch(new \App\Jobs\CompressVideo($review6, $company1, 'סנדויצ`ים מעוד טעימים', $user9));

        $user10->attachMedia($review7, 'אהבתי את המקום');
        $company2->attachMedia($review7, 'אהבתי את המקום');
        dispatch(new \App\Jobs\CompressVideo($review7, $company2, 'אהבתי את המקום', $user10));

        $user2->attachMedia($review8, 'סלטים טובים וטריים');
        $company2->attachMedia($review8, 'סלטים טובים וטריים');
        dispatch(new \App\Jobs\CompressVideo($review8, $company2, 'סלטים טובים וטריים', $user2));

        $user1->attachMedia($review9, 'בית מלון מעולה לזוגים צעירים');
        $company3->attachMedia($review9, 'בית מלון מעולה לזוגים צעירים');
        dispatch(new \App\Jobs\CompressVideo($review9, $company3, 'בית מלון מעולה לזוגים צעירים', $user1));

        $user7->attachMedia($review10, 'מקום יפה וטוב');
        $company3->attachMedia($review10, 'מקום יפה וטוב');
        dispatch(new \App\Jobs\CompressVideo($review10, $company3, 'מקום יפה וטוב', $user7));

        $user8->attachMedia($review11, 'ספא מצוין');
        $company9->attachMedia($review11, 'ספא מצוין');
        dispatch(new \App\Jobs\CompressVideo($review11, $company9, 'ספא מצוין', $user8));

        $user10->attachMedia($review12, 'אהבתי את התצוגה');
        $company6->attachMedia($review12, 'אהבתי את התצוגה');
        dispatch(new \App\Jobs\CompressVideo($review12, $company6, 'אהבתי את התצוגה', $user10));

        $user6->attachMedia($review13, 'התרבות שלנו');
        $company6->attachMedia($review13, 'התרבות שלנו');
        dispatch(new \App\Jobs\CompressVideo($review13, $company6, 'התרבות שלנו', $user6));

        $user4->attachMedia($review14, 'פיצה הכי טובה בארץ');
        $company4->attachMedia($review14, 'פיצה הכי טובה בארץ');
        dispatch(new \App\Jobs\CompressVideo($review14, $company4, 'פיצה הכי טובה בארץ', $user4));

        $user8->attachMedia($review15, 'פיצה מרגריטה זה משהו');
        $company4->attachMedia($review15, 'פיצה מרגריטה זה משהו');
        dispatch(new \App\Jobs\CompressVideo($review15, $company4, 'פיצה מרגריטה זה משהו', $user8));

        $user3->attachMedia($review16, 'דג טרי, אורז טעים');
        $company5->attachMedia($review16, 'דג טרי, אורז טעים');
        dispatch(new \App\Jobs\CompressVideo($review16, $company5, 'דג טרי, אורז טעים', $user3));

        $user5->attachMedia($review17, 'אהבתי את הסושי פה');
        $company5->attachMedia($review17, 'אהבתי את הסושי פה');
        dispatch(new \App\Jobs\CompressVideo($review17, $company5, 'אהבתי את הסושי פה', $user5));

        $user4->attachMedia($review18, 'מקום מה זה טעים');
        $company5->attachMedia($review18, 'מקום מה זה טעים');
        dispatch(new \App\Jobs\CompressVideo($review18, $company5, 'מקום מה זה טעים', $user4));

        $user1->attachMedia($review19, 'עובדים טוב-טוב');
        $company10->attachMedia($review19, 'עובדים טוב-טוב');
        dispatch(new \App\Jobs\CompressVideo($review19, $company10, 'עובדים טוב-טוב', $user1));

        $user8->attachMedia($review20, 'אפליקציות יפות ומהירות');
        $company10->attachMedia($review20, 'אפליקציות יפות ומהירות');
        dispatch(new \App\Jobs\CompressVideo($review20, $company10, 'אפליקציות יפות ומהירות', $user8));
    }
}
