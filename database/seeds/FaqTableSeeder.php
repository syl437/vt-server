<?php

use Illuminate\Database\Seeder;

class FaqTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $question = 'היכן השובר שלי?';
        $answer = '<p>על מנת לצפות בשובר שלכם,&nbsp;התחברו&nbsp;ולחצו על שימכם בפינה השמאלית בראש העמוד.<br />תחת "החשבון שלי" לחצו על "צפה בשובר" בסמוך לשובר אותו תרצו להדפיס. הוראות המימוש יופיעו תחת האותיות הקטנות.<br />אם הנכם משתמשים באפליקציה, לחצו על התפריט מצד ימין למעלה, ולאחר מכן על "הגרופונים שלי". שם תוכלו לצפות ולהדפיס את כל הגרופונים שלכם.<br /><br />לתשומת ליבכם, במרבית עסקאות המוצרים באתר, המוצר שרכשתם ישלח ישירות לכתובת המשלוח שעידכנתם במעמד הרכישה או לכתובת המעודכנת בחשבונכם, כך שאין צורך בהדפסת השובר. בחלק מהעסקאות יש ליצור קשר עם הספק לתיאום המשלוח.</p>';
        
        $q1 = new \App\Models\Faq();
        $q1->save();
        
        $q1->translateOrNew('en')->question = $question;
        $q1->translateOrNew('he')->question = $question;
        $q1->translateOrNew('en')->answer = $answer;
        $q1->translateOrNew('he')->answer = $answer;
        $q1->save();

        $q2 = new \App\Models\Faq();
        $q2->save();

        $q2->translateOrNew('en')->question = $question;
        $q2->translateOrNew('he')->question = $question;
        $q2->translateOrNew('en')->answer = $answer;
        $q2->translateOrNew('he')->answer = $answer;
        $q2->save();
        
        $q3 = new \App\Models\Faq();
        $q3->save();

        $q3->translateOrNew('en')->question = $question;
        $q3->translateOrNew('he')->question = $question;
        $q3->translateOrNew('en')->answer = $answer;
        $q3->translateOrNew('he')->answer = $answer;
        $q3->save();
    }
}
