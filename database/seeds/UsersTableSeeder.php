<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = new \App\Models\User();
        $user1->name = 'Evgenia Gofman';
        $user1->email = 'evgeniagofman@gmail.com';
        $user1->password = bcrypt(env('DEFAULT_PASSWORD'));
        $user1->phone = '+972524208555';
        $user1->country = 'IL';
        $user1->points = 500;
        $user1->api_token = '123';
        $user1->save();

        $user2 = new \App\Models\User();
        $user2->name = 'Ray Bradbury';
        $user2->email = 'raybradbury@test.com';
        $user2->password = bcrypt(env('DEFAULT_PASSWORD'));
        $user2->phone = '+972521111111';
        $user2->country = 'IL';
        $user2->points = 100;
        $user2->api_token = 'ray';
        $user2->save();

        $user3 = new \App\Models\User();
        $user3->name = 'Neil Gaiman';
        $user3->email = 'neilgaiman@test.com';
        $user3->password = bcrypt(env('DEFAULT_PASSWORD'));
        $user3->phone = '+9725222222222';
        $user3->country = 'IL';
        $user3->points = 200;
        $user3->api_token = 'neil';
        $user3->save();

        $user4 = new \App\Models\User();
        $user4->name = 'Stephen King';
        $user4->email = 'stephenking@test.com';
        $user4->password = bcrypt(env('DEFAULT_PASSWORD'));
        $user4->phone = '+972523333333';
        $user4->country = 'IL';
        $user4->points = 300;
        $user4->api_token = 'stephen';
        $user4->save();

        $user5 = new \App\Models\User();
        $user5->name = 'George R. R. Martin';
        $user5->email = 'georgemartin@test.com';
        $user5->password = bcrypt(env('DEFAULT_PASSWORD'));
        $user5->phone = '+972524444444';
        $user5->country = 'IL';
        $user5->points = 400;
        $user5->api_token = 'george';
        $user5->save();

        $user6 = new \App\Models\User();
        $user6->name = 'Alice Munro';
        $user6->email = 'alicemunro@test.com';
        $user6->password = bcrypt(env('DEFAULT_PASSWORD'));
        $user6->phone = '+972525555555';
        $user6->country = 'IL';
        $user6->points = 500;
        $user6->api_token = 'alice';
        $user6->save();

        $user7 = new \App\Models\User();
        $user7->name = 'Haruki Murakami';
        $user7->email = 'harukimurakami@test.com';
        $user7->password = bcrypt(env('DEFAULT_PASSWORD'));
        $user7->phone = '+972526666666';
        $user7->country = 'IL';
        $user7->points = 600;
        $user7->api_token = 'haruki';
        $user7->save();

        $user8 = new \App\Models\User();
        $user8->name = 'Chuck Palahniuk';
        $user8->email = 'chuckpalahniuk@test.com';
        $user8->password = bcrypt(env('DEFAULT_PASSWORD'));
        $user8->phone = '+972527777777';
        $user8->country = 'IL';
        $user8->points = 700;
        $user8->api_token = 'chuck';
        $user8->save();

        $user9 = new \App\Models\User();
        $user9->name = 'Joan K. Rowling';
        $user9->email = 'joanrowling@test.com';
        $user9->password = bcrypt(env('DEFAULT_PASSWORD'));
        $user9->phone = '+972528888888';
        $user9->country = 'IL';
        $user9->points = 800;
        $user9->api_token = 'joan';
        $user9->save();

        $user10 = new \App\Models\User();
        $user10->name = 'Donna Tartt';
        $user10->email = 'donnatartt@test.com';
        $user10->password = bcrypt(env('DEFAULT_PASSWORD'));
        $user10->phone = '+972529999999';
        $user10->country = 'IL';
        $user10->points = 900;
        $user10->api_token = 'donna';
        $user10->save();
    }
}
