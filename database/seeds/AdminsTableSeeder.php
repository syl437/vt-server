<?php

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new \App\Models\Admin();
        $admin->username = 'videotravel';
        $admin->email = 'evgeniagofman@gmail.com';
        $admin->password = bcrypt(env('DEFAULT_PASSWORD'));
        $admin->phone = '0522222222';
        $admin->save();
    }
}
