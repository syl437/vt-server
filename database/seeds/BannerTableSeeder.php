<?php

use Illuminate\Database\Seeder;

class BannerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $banner1 = new \App\Models\Banner();
        $banner1->type = 'explanation';
        $banner1->save();

        $banner2 = new \App\Models\Banner();
        $banner2->type = 'ad';
        $banner2->company_id = 1;
        $banner2->save();

        $banner3 = new \App\Models\Banner();
        $banner3->type = 'ad';
        $banner3->company_id = 4;
        $banner3->save();

        $banner4 = new \App\Models\Banner();
        $banner4->type = 'ad';
        $banner4->company_id = 7;
        $banner4->save();
    }
}
