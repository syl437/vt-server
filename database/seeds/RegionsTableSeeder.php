<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RegionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('regions')->insert(
            [
                ['created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
                ['created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
                ['created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
                ['created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
                ['created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
                ['created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
                ['created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
                ['created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
                ['created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
                ['created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
                ['created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
                ['created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
                ['created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
                ['created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
                ['created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
                ['created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
                ['created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
                ['created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
                ['created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
                ['created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
                ['created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
                ['created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')]
            ]
        );


        $c1 = \App\Models\Region::find(1);
        $c1->translateOrNew('en')->title = 'אצבע הגליל';
        $c1->translateOrNew('he')->title = 'אצבע הגליל';
        $c1->save();

        $c2 = \App\Models\Region::find(2);
        $c2->translateOrNew('en')->title = 'גליל עליון';
        $c2->translateOrNew('he')->title = 'גליל עליון';
        $c2->save();

        $c3 = \App\Models\Region::find(3);
        $c3->translateOrNew('en')->title = 'גליל תחתון';
        $c3->translateOrNew('he')->title = 'גליל תחתון';
        $c3->save();

        $c4 = \App\Models\Region::find(4);
        $c4->translateOrNew('en')->title = 'גליל מערבי';
        $c4->translateOrNew('he')->title = 'גליל מערבי';
        $c4->save();

        $c5 = \App\Models\Region::find(5);
        $c5->translateOrNew('en')->title = 'רמת הגולן';
        $c5->translateOrNew('he')->title = 'רמת הגולן';
        $c5->save();

        $c6 = \App\Models\Region::find(6);
        $c6->translateOrNew('en')->title = 'סובב כינרת';
        $c6->translateOrNew('he')->title = 'סובב כינרת';
        $c6->save();

        $c7 = \App\Models\Region::find(7);
        $c7->translateOrNew('en')->title = 'חיפה';
        $c7->translateOrNew('he')->title = 'חיפה';
        $c7->save();

        $c8 = \App\Models\Region::find(8);
        $c8->translateOrNew('en')->title = 'כרמלים';
        $c8->translateOrNew('he')->title = 'כרמלים';
        $c8->save();

        $c9 = \App\Models\Region::find(9);
        $c9->translateOrNew('en')->title = 'מישור החוף';
        $c9->translateOrNew('he')->title = 'מישור החוף';
        $c9->save();

        $c10 = \App\Models\Region::find(10);
        $c10->translateOrNew('en')->title = 'השרון';
        $c10->translateOrNew('he')->title = 'השרון';
        $c10->save();

        $c11 = \App\Models\Region::find(11);
        $c11->translateOrNew('en')->title = 'תל אביב';
        $c11->translateOrNew('he')->title = 'תל אביב';
        $c11->save();

        $c12 = \App\Models\Region::find(12);
        $c12->translateOrNew('en')->title = 'מרכז';
        $c12->translateOrNew('he')->title = 'מרכז';
        $c12->save();

        $c13 = \App\Models\Region::find(13);
        $c13->translateOrNew('en')->title = 'אשקלון';
        $c13->translateOrNew('he')->title = 'אשקלון';
        $c13->save();

        $c14 = \App\Models\Region::find(14);
        $c14->translateOrNew('en')->title = 'אשדוד';
        $c14->translateOrNew('he')->title = 'אשדוד';
        $c14->save();

        $c15 = \App\Models\Region::find(15);
        $c15->translateOrNew('en')->title = 'נגב';
        $c15->translateOrNew('he')->title = 'נגב';
        $c15->save();

        $c16 = \App\Models\Region::find(16);
        $c16->translateOrNew('en')->title = 'מצפה רמון';
        $c16->translateOrNew('he')->title = 'מצפה רמון';
        $c16->save();

        $c17 = \App\Models\Region::find(17);
        $c17->translateOrNew('en')->title = 'הרי אילת';
        $c17->translateOrNew('he')->title = 'הרי אילת';
        $c17->save();

        $c18 = \App\Models\Region::find(18);
        $c18->translateOrNew('en')->title = 'אילת';
        $c18->translateOrNew('he')->title = 'אילת';
        $c18->save();

        $c19 = \App\Models\Region::find(19);
        $c19->translateOrNew('en')->title = 'ים המלח';
        $c19->translateOrNew('he')->title = 'ים המלח';
        $c19->save();

        $c20 = \App\Models\Region::find(20);
        $c20->translateOrNew('en')->title = 'מדבר יהודה';
        $c20->translateOrNew('he')->title = 'מדבר יהודה';
        $c20->save();

        $c21 = \App\Models\Region::find(21);
        $c21->translateOrNew('en')->title = 'ירושלים';
        $c21->translateOrNew('he')->title = 'ירושלים';
        $c21->save();

        $c22 = \App\Models\Region::find(22);
        $c22->translateOrNew('en')->title = 'סובב ירושלים';
        $c22->translateOrNew('he')->title = 'סובב ירושלים';
        $c22->save();


    }
}
