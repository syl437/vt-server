<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CompanyCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('company_categories')->insert(
            [
                ['created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
                ['created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
                ['created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
                ['created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')]
            ]);

        $c1 = \App\Models\CompanyCategory::find(1);
        $c1->translateOrNew('en')->title = 'Hotels';
        $c1->translateOrNew('he')->title = 'בתי מלון';
        $c1->save();

        $c2 = \App\Models\CompanyCategory::find(2);
        $c2->translateOrNew('en')->title = 'Restaurants';
        $c2->translateOrNew('he')->title = 'מסעדות';
        $c2->save();

        $c3 = \App\Models\CompanyCategory::find(3);
        $c3->translateOrNew('en')->title = 'Attractions';
        $c3->translateOrNew('he')->title = 'אטרקציות';
        $c3->save();

        $c4 = \App\Models\CompanyCategory::find(4);
        $c4->translateOrNew('en')->title = 'Fun';
        $c4->translateOrNew('he')->title = 'בילויים';
        $c4->save();

    }
}
