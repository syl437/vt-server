<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CompanySubcategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('company_subcategories')->insert(
            [
                ['company_category_id' => 1, 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
                ['company_category_id' => 1, 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
                ['company_category_id' => 1, 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
                ['company_category_id' => 2, 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
                ['company_category_id' => 2, 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
                ['company_category_id' => 2, 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
                ['company_category_id' => 2, 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
                ['company_category_id' => 2, 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
                ['company_category_id' => 3, 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
                ['company_category_id' => 3, 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
                ['company_category_id' => 3, 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
                ['company_category_id' => 4, 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
                ['company_category_id' => 4, 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
                ['company_category_id' => 4, 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
            ]);

        $c1 = \App\Models\CompanySubcategory::find(1);
        $c1->translateOrNew('en')->title = 'Apartments';
        $c1->translateOrNew('he')->title = 'צימרים';
        $c1->save();

        $c2 = \App\Models\CompanySubcategory::find(2);
        $c2->translateOrNew('en')->title = 'Hotels SPA';
        $c2->translateOrNew('he')->title = 'ספא';
        $c2->save();

        $c3 = \App\Models\CompanySubcategory::find(3);
        $c3->translateOrNew('en')->title = 'Boutique hotels';
        $c3->translateOrNew('he')->title = 'בוטיק';
        $c3->save();

        $c4 = \App\Models\CompanySubcategory::find(4);
        $c4->translateOrNew('en')->title = 'Bar/Pub';
        $c4->translateOrNew('he')->title = 'בר/פאב';
        $c4->save();

        $c5 = \App\Models\CompanySubcategory::find(5);
        $c5->translateOrNew('en')->title = 'Pizza';
        $c5->translateOrNew('he')->title = 'פיצריה';
        $c5->save();

        $c6 = \App\Models\CompanySubcategory::find(6);
        $c6->translateOrNew('en')->title = 'Burgers';
        $c6->translateOrNew('he')->title = 'בורגרים';
        $c6->save();

        $c7 = \App\Models\CompanySubcategory::find(7);
        $c7->translateOrNew('en')->title = 'Sushi';
        $c7->translateOrNew('he')->title = 'סושי';
        $c7->save();

        $c8 = \App\Models\CompanySubcategory::find(8);
        $c8->translateOrNew('en')->title = 'Coffee';
        $c8->translateOrNew('he')->title = 'בית קפה';
        $c8->save();

        $c9 = \App\Models\CompanySubcategory::find(9);
        $c9->translateOrNew('en')->title = 'Museums';
        $c9->translateOrNew('he')->title = 'מוזיאונים';
        $c9->save();

        $c10 = \App\Models\CompanySubcategory::find(10);
        $c10->translateOrNew('en')->title = 'Luna Park';
        $c10->translateOrNew('he')->title = 'לונה פרק';
        $c10->save();

        $c11 = \App\Models\CompanySubcategory::find(11);
        $c11->translateOrNew('en')->title = 'Escape rooms';
        $c11->translateOrNew('he')->title = 'חדרי בריחה';
        $c11->save();

        $c12 = \App\Models\CompanySubcategory::find(12);
        $c12->translateOrNew('en')->title = 'Kayaks';
        $c12->translateOrNew('he')->title = 'קייקים';
        $c12->save();

        $c13 = \App\Models\CompanySubcategory::find(13);
        $c13->translateOrNew('en')->title = 'Tours';
        $c13->translateOrNew('he')->title = 'טיולים';
        $c13->save();

        $c14 = \App\Models\CompanySubcategory::find(14);
        $c14->translateOrNew('en')->title = 'App development';
        $c14->translateOrNew('he')->title = 'פיתוח אפליקציות';
        $c14->save();

    }
}
