<?php

use Illuminate\Database\Seeder;

class CouponsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $desc = 'רכישה ומימוש ללא הגבלה, בכפוף למלאי הזמין,
חנייה מסודרת חינם.
לא תקף לסדנאות שף אורח, סדנת 750 גרם בשר וסדנת פיוז\'ן בשרים.
מדיניות ביטולים: 
בכפוף לתנאי השימוש של גרופון (סעיף 24) ולהוראות חוק הגנת הצרכן.
הגרופון יהיה זמין בתום הרכישה תחת \'ההזמנות שלי\'.
ראו את הכללים החלים על כל העסקאות.
בית העסק (ולא גרופון) הנו האחראי הבלעדי לאיכות, לטיב ולטיפול במוצרים ו/או בשירותים שפורסמו.
';
        
        $coupon1 = new \App\Models\Coupon();
        $coupon1->price = 10;
        $coupon1->company()->associate(\App\Models\Company::find(1));
        $coupon1->quantity = 10;
        $coupon1->save();

        $coupon1->translateOrNew('en')->title = 'ארוחת בוקר חינם';
        $coupon1->translateOrNew('he')->title = 'ארוחת בוקר חינם';
        $coupon1->translateOrNew('en')->description = $desc;
        $coupon1->translateOrNew('he')->description = $desc;
        $coupon1->save();

        $coupon11 = new \App\Models\Coupon();
        $coupon11->price = 10;
        $coupon11->company()->associate(\App\Models\Company::find(1));
        $coupon11->quantity = 10;
        $coupon11->save();

        $coupon11->translateOrNew('en')->title = 'קפה ומאפה חינם';
        $coupon11->translateOrNew('he')->title = 'קפה ומאפה חינם';
        $coupon11->translateOrNew('en')->description = $desc;
        $coupon11->translateOrNew('he')->description = $desc;
        $coupon11->save();

        $coupon2 = new \App\Models\Coupon();
        $coupon2->price = 20;
        $coupon2->company()->associate(\App\Models\Company::find(2));
        $coupon2->quantity = 20;
        $coupon2->save();

        $coupon2->translateOrNew('en')->title = 'ארוחה זוגית 2+2';
        $coupon2->translateOrNew('he')->title = 'ארוחה זוגית 2+2';
        $coupon2->translateOrNew('en')->description = $desc;
        $coupon2->translateOrNew('he')->description = $desc;
        $coupon2->save();

        $coupon22 = new \App\Models\Coupon();
        $coupon22->price = 20;
        $coupon22->company()->associate(\App\Models\Company::find(2));
        $coupon22->quantity = 20;
        $coupon22->save();

        $coupon22->translateOrNew('en')->title = 'סנדויץ טונה חינם';
        $coupon22->translateOrNew('he')->title = 'סנדויץ טונה חינם';
        $coupon22->translateOrNew('en')->description = $desc;
        $coupon22->translateOrNew('he')->description = $desc;
        $coupon22->save();

        $coupon3 = new \App\Models\Coupon();
        $coupon3->price = 30;
        $coupon3->company()->associate(\App\Models\Company::find(3));
        $coupon3->quantity = 30;
        $coupon3->save();

        $coupon3->translateOrNew('en')->title = 'צ`יסר ויסקי בבר';
        $coupon3->translateOrNew('he')->title = 'צ`יסר ויסקי בבר';
        $coupon3->translateOrNew('en')->description = $desc;
        $coupon3->translateOrNew('he')->description = $desc;
        $coupon3->save();

        $coupon33 = new \App\Models\Coupon();
        $coupon33->price = 30;
        $coupon33->company()->associate(\App\Models\Company::find(3));
        $coupon33->quantity = 30;
        $coupon33->save();

        $coupon33->translateOrNew('en')->title = 'ספא 1+1';
        $coupon33->translateOrNew('he')->title = 'ספא 1+1';
        $coupon33->translateOrNew('en')->description = $desc;
        $coupon33->translateOrNew('he')->description = $desc;
        $coupon33->save();
        
        $coupon4 = new \App\Models\Coupon();
        $coupon4->price = 40;
        $coupon4->company()->associate(\App\Models\Company::find(4));
        $coupon4->quantity = 40;
        $coupon4->save();

        $coupon4->translateOrNew('en')->title = 'פיצה אישית חינם';
        $coupon4->translateOrNew('he')->title = 'פיצה אישית חינם';
        $coupon4->translateOrNew('en')->description = $desc;
        $coupon4->translateOrNew('he')->description = $desc;
        $coupon4->save();

        $coupon44 = new \App\Models\Coupon();
        $coupon44->price = 40;
        $coupon44->company()->associate(\App\Models\Company::find(4));
        $coupon44->quantity = 40;
        $coupon44->save();

        $coupon44->translateOrNew('en')->title = 'פיצה משפחתית 50% הנחה';
        $coupon44->translateOrNew('he')->title = 'פיצה משפחתית 50% הנחה';
        $coupon44->translateOrNew('en')->description = $desc;
        $coupon44->translateOrNew('he')->description = $desc;
        $coupon44->save();

        $coupon5 = new \App\Models\Coupon();
        $coupon5->price = 50;
        $coupon5->company()->associate(\App\Models\Company::find(5));
        $coupon5->quantity = 50;
        $coupon5->save();

        $coupon5->translateOrNew('en')->title = 'ניגירי סלמון חינם';
        $coupon5->translateOrNew('he')->title = 'ניגירי סלמון חינם';
        $coupon5->translateOrNew('en')->description = $desc;
        $coupon5->translateOrNew('he')->description = $desc;
        $coupon5->save();

        $coupon55 = new \App\Models\Coupon();
        $coupon55->price = 50;
        $coupon55->company()->associate(\App\Models\Company::find(5));
        $coupon55->quantity = 50;
        $coupon55->save();

        $coupon55->translateOrNew('en')->title = '50% הנחה אחרי 22.00';
        $coupon55->translateOrNew('he')->title = '50% הנחה אחרי 22.00';
        $coupon55->translateOrNew('en')->description = $desc;
        $coupon55->translateOrNew('he')->description = $desc;
        $coupon55->save();

        $coupon6 = new \App\Models\Coupon();
        $coupon6->price = 60;
        $coupon6->company()->associate(\App\Models\Company::find(6));
        $coupon6->quantity = 60;
        $coupon6->save();

        $coupon6->translateOrNew('en')->title = 'כניסה חינם לילדים עד גיל 14';
        $coupon6->translateOrNew('he')->title = 'כניסה חינם לילדים עד גיל 14';
        $coupon6->translateOrNew('en')->description = $desc;
        $coupon6->translateOrNew('he')->description = $desc;
        $coupon6->save();

        $coupon66 = new \App\Models\Coupon();
        $coupon66->price = 60;
        $coupon66->company()->associate(\App\Models\Company::find(6));
        $coupon66->quantity = 60;
        $coupon66->save();

        $coupon66->translateOrNew('en')->title = 'כרטיס 1+1';
        $coupon66->translateOrNew('he')->title = 'כרטיס 1+1';
        $coupon66->translateOrNew('en')->description = $desc;
        $coupon66->translateOrNew('he')->description = $desc;
        $coupon66->save();

        $coupon7 = new \App\Models\Coupon();
        $coupon7->price = 70;
        $coupon7->company()->associate(\App\Models\Company::find(7));
        $coupon7->quantity = 70;
        $coupon7->save();

        $coupon7->translateOrNew('en')->title = 'כוס בירה חינם';
        $coupon7->translateOrNew('he')->title = 'כוס בירה חינם';
        $coupon7->translateOrNew('en')->description = $desc;
        $coupon7->translateOrNew('he')->description = $desc;
        $coupon7->save();

        $coupon77 = new \App\Models\Coupon();
        $coupon77->price = 70;
        $coupon77->company()->associate(\App\Models\Company::find(7));
        $coupon77->quantity = 70;
        $coupon77->save();

        $coupon77->translateOrNew('en')->title = 'צ`סיר ויסקי חינם';
        $coupon77->translateOrNew('he')->title = 'צ`סיר ויסקי חינם';
        $coupon77->translateOrNew('en')->description = $desc;
        $coupon77->translateOrNew('he')->description = $desc;
        $coupon77->save();

        $coupon8 = new \App\Models\Coupon();
        $coupon8->price = 80;
        $coupon8->company()->associate(\App\Models\Company::find(8));
        $coupon8->quantity = 80;
        $coupon8->save();

        $coupon8->translateOrNew('en')->title = '50% הנחה על הסנדויצ`ים';
        $coupon8->translateOrNew('he')->title = '50% הנחה על הסנדויצ`ים';
        $coupon8->translateOrNew('en')->description = $desc;
        $coupon8->translateOrNew('he')->description = $desc;
        $coupon8->save();

        $coupon88 = new \App\Models\Coupon();
        $coupon88->price = 80;
        $coupon88->company()->associate(\App\Models\Company::find(8));
        $coupon88->quantity = 80;
        $coupon88->save();

        $coupon88->translateOrNew('en')->title = 'ארוחה 1+1';
        $coupon88->translateOrNew('he')->title = 'ארוחה 1+1';
        $coupon88->translateOrNew('en')->description = $desc;
        $coupon88->translateOrNew('he')->description = $desc;
        $coupon88->save();

        $coupon9 = new \App\Models\Coupon();
        $coupon9->price = 90;
        $coupon9->company()->associate(\App\Models\Company::find(9));
        $coupon9->quantity = 90;
        $coupon9->save();

        $coupon9->translateOrNew('en')->title = 'ארוחת בוקר חינם';
        $coupon9->translateOrNew('he')->title = 'ארוחת בוקר חינם';
        $coupon9->translateOrNew('en')->description = $desc;
        $coupon9->translateOrNew('he')->description = $desc;
        $coupon9->save();

        $coupon99 = new \App\Models\Coupon();
        $coupon99->price = 90;
        $coupon99->company()->associate(\App\Models\Company::find(9));
        $coupon99->quantity = 90;
        $coupon99->save();

        $coupon99->translateOrNew('en')->title = 'יום ספא 50% הנחה';
        $coupon99->translateOrNew('he')->title = 'יום ספא 50% הנחה';
        $coupon99->translateOrNew('en')->description = $desc;
        $coupon99->translateOrNew('he')->description = $desc;
        $coupon99->save();

        $coupon10 = new \App\Models\Coupon();
        $coupon10->price = 100;
        $coupon10->company()->associate(\App\Models\Company::find(10));
        $coupon10->quantity = 100;
        $coupon10->save();

        $coupon10->translateOrNew('en')->title = 'עיצוב לאפליקציה 50% הנחה';
        $coupon10->translateOrNew('he')->title = 'עיצוב לאפליקציה 50% הנחה';
        $coupon10->translateOrNew('en')->description = $desc;
        $coupon10->translateOrNew('he')->description = $desc;
        $coupon10->save();

        $coupon100 = new \App\Models\Coupon();
        $coupon100->price = 100;
        $coupon100->company()->associate(\App\Models\Company::find(10));
        $coupon100->quantity = 100;
        $coupon100->save();

        $coupon100->translateOrNew('en')->title = 'עוד קופון אין לי כוח כבר';
        $coupon100->translateOrNew('he')->title = 'עוד קופון אין לי כוח כבר';
        $coupon100->translateOrNew('en')->description = $desc;
        $coupon100->translateOrNew('he')->description = $desc;
        $coupon100->save();
    }
}
