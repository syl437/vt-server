<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Error Message Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the Laravel Responder package.
    | When it generates error responses, it will search the messages array
    | below for any key matching the given error code for the response.
    |
    */

    'resource_not_found' => 'The requested resource does not exist.',
    'unauthenticated' => 'You are not authenticated for this request.',
    'unauthorized' => 'You are not authorized for this request.',
    'relation_not_found' => 'The requested relation does not exist.',
    'validation_failed' => 'The given data failed to pass validation.',
    'invalid_phone' => 'The phone number is not valid. Please check again.',
    'no_code' => 'There is no such code. Please check again.',
    'invalid_coupon' => 'This coupon is invalid.',
    'already_rated' => 'You have already rate this!',
    'needs_approval' => 'The company still needs approval! Please contact the support.',
    'invalid_email' => 'Email is not valid!',
    'invalid_credentials' => 'Credentials are not valid!',
    'token_invalid' => 'Please log in once again!',
    'invalid_old_password' => 'Your old password is invalid. Please check it again.',
    'no_files' => 'No files attached!',
    'email_already_exists' => 'Email already exists in the database!',
    'already_owner' => 'This company already has an owner!',
    'no_owner' => 'This company has no owner yet, please make an owner first.',
    'no_user' => 'No user with such credentials.',

];