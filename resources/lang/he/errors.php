<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Error Message Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the Laravel Responder package.
    | When it generates error responses, it will search the messages array
    | below for any key matching the given error code for the response.
    |
    */

    'resource_not_found' => 'הבקשה לא קיימת',
    'unauthenticated' => 'אתה לא מאומת לבקשה הזו',
    'unauthorized' => 'אתה לא מורשה לבקשה הזו',
    'validation_failed' => 'המידע לא נכון',
    'invalid_phone' => 'מסםר הטלפון לא תקין , אנא נסה שנית',
    'no_code' => 'לא קיים קוד כזה במערכת אנא נסה שנית',
    'invalid_coupon' => 'קופון לא קיים',
    'already_rated' => 'דירג את זה בעבר',
    'needs_approval' => 'החברה עדיים צריך לאשר את זה בבקשה צור קשר עם התמיכה',
    'invalid_email' => 'אימייל לא תקין',
    'invalid_credentials' => 'אימייל וסיסמה לא קיימים',
    'token_invalid' => 'בבקשה התחבר שוב',
    'invalid_old_password' => 'הסיסמה הישנה שלך לא נכונה בבקשה נסה שוב',
    'no_files' => 'לא מצורפים קבצים',
    'email_already_exists' => 'האימייל הזה קיים אנא נסה מייל אחר',
    'already_owner' => 'לחברה הזו יש בעלים אנא נסה שנית',
    'no_owner' => 'לחברה אין בעלים , בבקשה מלא פרטים',
    'no_user' => 'אין משתמש עם אימייל וסיסמה כעלה',

];