<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>YTravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>

        body {
            margin: 0;
        }

        .color-background {
            background-color: #690079;
            padding: 10px 0;
        }

        .logo-image {
            width: 70%;
            margin: 0 auto;
        }

        .color-text {
            color: #690079;
        }

    </style>

</head>
<body>

<div class="container">
    <div class="row color-background text-center">
        <div class="col-12">
            <img src="images/header_logo.png" class="logo-image">
        </div>
    </div>
    <div class="row">
        <div class="col-12 p-0">
            <iframe style="height: 30vh; width: 100%;" src="https://www.youtube.com/embed/693htHBw6S0?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        </div>
    </div>
    <div class="row">
        <div class="col-2 text-center p-0 pt-3">
            <img src="images/coin.png" width="50">
        </div>

        @if ($lang != 'he')
        <div class="col-8 text-center p-0">
            <h4 class="color-text"><b>Welcome to YTravel</b></h4>
            <div>Hotel {{$name}} is happy to give you</div>
            <h4 class="color-text"><b>{{$points}}$</b></h4>
            <div>to shop your resort area.</div>
            <div>In order to receive money,</div>
            <div>please download the application</div>
            <div>at the following link:</div>
            <div>
            <div class="py-2">
                <a href="http://tapper.co.il/ytravel.apk">
                   <img src="images/download.png" style="width: 48%;">
                </a>
                <a href="https://itunes.apple.com/us/app/ytravel/id1362024077?mt=8">
                    <img src="images/download_apple.png" style="width: 48%;">
                </a>
            </div>
            <div>And enter the code: {{$code}}</div>
            <div>from the main page.</div>
            <h4 class="color-text"><b>Have a good vacation!</b></h4>
        </div>
        @endif

        @if ($lang == 'he')
        <div class="col-8 text-center p-0">
            <h4 class="color-text"><b>ברוכים הבאים</b></h4>
            <div>בית מלון {{$name}} שמח להעניק לכם</div>
            <h4 class="color-text"><b>{{$points}}$</b></h4>
            <div>לקניות באזור נופש שלכם</div>
            <div>על מנת לקבל את הכסף</div>
            <div>יש להוריד את האפליקציה</div>
            <div>בקישור הבא</div>
            <div class="py-2">
                <a href="https://play.google.com/store/apps/details?id=com.tapper.ytravel">
                    <img src="images/download.png" style="width: 48%;">
                </a>
                <a href="https://itunes.apple.com/us/app/ytravel/id1362024077?mt=8">
                    <img src="images/download_apple.png" style="width: 48%;">
                </a>
            </div>
            <div><span>ולהזין את הקוד</span>: <b class="color-text">{{$code}}</b></div>
            <div>בהכנס קוד בעמוד הראשי</div>
            <h4 class="color-text"><b>חופשה נעימה.</b></h4>
        </div>
        @endif

        <div class="col-2 text-center p-0 pt-3">
            <img src="images/coin.png" width="50">
        </div>
    </div>
</div>

</body>
</html>
