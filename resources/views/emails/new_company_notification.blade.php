<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>

<div>Hello Admin!</div>

<div>The new company was added: </div>

<div>Title: {{$company->title}}</div>
<div>Address: {{$company->address}}</div>
<div>Category: {{$company->category->title}}</div>
<div>Subcategory: {{$company->subcategory->title}}</div>
<div>Phone: {{$company->phone}}</div>
<div>Description: {{$company->description}}</div>

<div>Don't forget to approve it!</div>

</body>
</html>
