<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>

<div>Hello Admin!</div>

<div>There is new user in your application: </div>
<div>Name: {{$user->name}}</div>
<div>Email: {{$user->email}}</div>
<div>Phone: {{$user->phone}}</div>
<div>Country: {{$user->country}}</div>

</body>
</html>
