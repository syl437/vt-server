<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>

<div>Hello Admin!</div>

<div>The new video was added for this company: {{$link}}</div>
<div>By this user: {{$user->name}}, email: {{$user->email}}, phone: {{$user->phone}}</div>

<div>Don't forget to approve it!</div>

</body>
</html>
