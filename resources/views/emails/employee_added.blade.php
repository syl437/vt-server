<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>

<div>Hello from YTravel Admin! </div>

<div>You are connected now to the {{$company}}!</div>

<div>Username: {{$email}}</div>
<div>Password: {{$password}}</div>

<div>You can change password in settings.</div>

<div>Welcome!</div>

</body>
</html>
