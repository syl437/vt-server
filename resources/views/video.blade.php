<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta property="og:type" content="video" />
    <meta property="og:video:url" content="{{$url}}" />
    <meta property="og:image" content="{{$image}}" />
    <meta property="og:video" content="{{$url}}" />
    <meta property="og:title" content="{{$title}}" />
    <meta property="og:description" content="{{$description}}" />

    <title>YTravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>

        body {
            margin: 0;
        }

    </style>

</head>
<body>

<div class="container" dir="{{$dir}}">

    <div class="row">

        <div class="col-3">
            <img src="{{$logo}}" style="width: 12vh; padding: 10px;">
        </div>

        <div class="col-9">
            <div><b>{{$title}}</b></div>
            <div>{{$description}}</div>
        </div>

    </div>

    <div class="row">

        <div class="col-12">
            <video controls src="{{$url}}" poster="{{$image}}" style="width: 100%;"></video>
        </div>

    </div>

    <div class="row">
        <div class="col-12">
            <div class="py-2">
                <a href="https://play.google.com/store/apps/details?id=com.tapper.ytravel">
                    <img src="images/download.png" style="width: 48%; max-width: 200px;">
                </a>
                <a href="https://itunes.apple.com/us/app/ytravel/id1362024077?mt=8">
                    <img src="images/download_apple.png" style="width: 48%; max-width: 200px;">
                </a>
            </div>
        </div>
    </div>

</div>

</body>
</html>
