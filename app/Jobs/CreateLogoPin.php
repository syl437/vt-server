<?php

namespace App\Jobs;

use App\Models\File;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Intervention\Image\ImageManagerStatic;
use Plank\Mediable\Media;
use Plank\Mediable\MediaUploaderFacade;
use Image;

class CreateLogoPin implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var File
     */
    protected $media;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Media $media)
    {
        $this->media = $media;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $unix = time();
        $name = "pin_logo_{$this->media->id}_{$unix}.png";

        $image = Image::make($this->media->getAbsolutePath())->resize(30, 30);
        $circle = Image::canvas(30, 30);
        $circle->circle(30, 15, 15, function ($draw) {$draw->background('#ffffff');});

        $circle_image = $image->mask($circle, true);

        $pin = Image::make(resource_path('images/pin.png'));

        $pin->insert($circle_image, '', 5, 4)->save(storage_path("media/{$name}"));

        $imported_file = MediaUploaderFacade::importPath('media', $name);
        $file = File::find($this->media->id);

        $file->attachMedia($imported_file, 'pin');

        $imported_file->status = 'uploaded';
        $imported_file->save();
    }
}
