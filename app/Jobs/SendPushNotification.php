<?php

namespace App\Jobs;

use App\Models\Notification;
use OneSignal;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;


class SendPushNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;
    protected $text;
    protected $schedule;
    protected $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user, $text, $schedule, $data)
    {
        $this->user = $user;
        $this->text = $text;
        $this->schedule = $schedule;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        OneSignal::sendNotificationToUser($this->text, $this->user->push_id, null, $this->data, null, $this->schedule);

        if ($this->data['topic'] != 'registration'){
            $notification = new Notification();
            $notification->content = $this->text;
            $notification->user()->associate($this->user);
            $notification->save();
        }

    }
}
