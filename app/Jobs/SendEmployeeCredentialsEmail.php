<?php

namespace App\Jobs;

use App\Mail\EmployeeAdded;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendEmployeeCredentialsEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $email;
    protected $password;
    protected $company;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $password, $company)
    {
        $this->email = $email;
        $this->password = $password;
        $this->company = $company;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->email)->send(new EmployeeAdded($this->email, $this->password, $this->company));
    }
}
