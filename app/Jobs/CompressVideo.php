<?php

namespace App\Jobs;

use App\Models\File;
use FFMpeg\Format\Video\Ogg;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use FFMpeg\Format\Video\X264;
use Plank\Mediable\MediaUploaderFacade;

class CompressVideo implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 300;
    protected $media;
    protected $company;
    protected $type;
    protected $user;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($media, $company, $type, $user)
    {
        $this->media = $media;
        $this->company = $company;
        $this->type = $type;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $unix = time();
        $new = NULL;

        if ($this->type === 'main_video')
            $name = "main_video_company_{$this->company->id}_{$unix}";
        else
            $name = "review_company_{$this->company->id}_user_{$this->user->id}_{$unix}";

        if ($this->media->extension === '3gp'){

            \FFMpeg::fromDisk('media')
                ->open($this->media->getDiskPath())
                ->export()
                ->inFormat(new X264('aac', 'libx264'))
                ->save("original/{$name}.mp4");

            $new = \Plank\Mediable\MediaUploaderFacade::importPath("media", "original/{$name}.mp4");

        }

        $audioCodec = $new !== NULL ? 'aac' : 'libmp3lame';
        $format = with(new X264($audioCodec, 'libx264'))->setAdditionalParameters(['-vf', 'scale=640:360']);

        if ($new !== NULL){
            \FFMpeg::fromDisk('media')->open($new->getDiskPath())->export()->inFormat($format)->save("compressed/{$name}.mp4");
        } else {
            \FFMpeg::fromDisk('media')->open($this->media->getDiskPath())->export()->inFormat($format)->save("compressed/{$name}.mp4");
        }

        $compressed = \Plank\Mediable\MediaUploaderFacade::importPath("media", "compressed/{$name}.mp4");
        $compressed->status = 'uploaded';
        $compressed->compressed = 1;
        $compressed->save();

        if ($this->type === 'main_video'){
            $this->company->syncMedia($compressed, $this->type);
        } else {
            $this->user->syncMedia($compressed, $this->type);
            $this->company->syncMedia($compressed, $this->type);
        }

        $name_thumbnail = "thumbnail_video_{$compressed->id}_{$unix}.png";

        \FFMpeg::open($compressed->getDiskPath())
            ->getFrameFromSeconds(1)
            ->export()
            ->toDisk('media')
            ->save($name_thumbnail);

        $thumbnail = MediaUploaderFacade::importPath('media', $name_thumbnail);
        $video = File::find($compressed->id);

        $video->attachMedia($thumbnail, 'thumbnail');

        $thumbnail->status = 'uploaded';
        $thumbnail->save();
    }
}
