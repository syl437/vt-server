<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Nexmo\Laravel\Facade\Nexmo;

class SendSMS implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $msg;
    protected $phone;
    protected $country;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($msg, $phone, $country)
    {
        $this->msg = $msg;
        $this->phone = $phone;
        $this->country = $country;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->country === 'US'){
            Nexmo::message()->send(['from' => '12013814717', 'to' => $this->phone, 'text' => $this->msg, 'type' => 'unicode']);
//            Nexmo::message()->send(['from' => '12014847573', 'to' => $this->phone, 'text' => $this->msg, 'type' => 'unicode']);
        } else {
            Nexmo::message()->send(['from' => 'YTRAVEL', 'to' => $this->phone, 'text' => $this->msg, 'type' => 'unicode']);
        }
    }
}
