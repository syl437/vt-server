<?php

namespace App\Jobs;

use App\Mail\NewVideo;
use App\Models\Admin;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendVideoNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $company;
    protected $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($company, $user)
    {
        $this->company = $company;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $link = 'http://ytravel.kartisim.co.il/companies/' . $this->company . '/videos';
        $admin = Admin::find(1);

         Mail::to($admin->email)->send(new NewVideo($link, $this->user));

    }
}
