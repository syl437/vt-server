<?php

namespace App\Jobs;

use App\Mail\NewCompanyNotification;
use App\Models\Admin;
use App\Models\CompanyCategory;
use App\Models\CompanySubcategory;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendNewCompanyNotificationEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $company;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($company)
    {
        $this->company = $company;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $admin = Admin::find(1);

        $this->company->subcategory = CompanySubcategory::find($this->company->company_subcategory_id);
        $this->company->category = CompanyCategory::find($this->company->company_category_id);

        Mail::to($admin->email)->send(new NewCompanyNotification($this->company));
    }
}
