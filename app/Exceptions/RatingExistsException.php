<?php

namespace App\Exceptions;

use Flugg\Responder\Exceptions\Http\ApiException;

class RatingExistsException extends ApiException
{
    protected $errorCode = 428;
}