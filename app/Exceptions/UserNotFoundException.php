<?php

namespace App\Exceptions;


use Flugg\Responder\Exceptions\Http\ApiException;

class UserNotFoundException extends ApiException
{
    protected $errorCode = 425;
}