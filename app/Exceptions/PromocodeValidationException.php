<?php

namespace App\Exceptions;


use Flugg\Responder\Exceptions\Http\ApiException;

class PromocodeValidationException extends ApiException
{
    protected $errorCode = 422;
}