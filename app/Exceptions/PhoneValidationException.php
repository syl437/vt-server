<?php

namespace App\Exceptions;

use Flugg\Responder\Exceptions\Http\ApiException;

class PhoneValidationException extends ApiException
{
    protected $errorCode = 427;
}