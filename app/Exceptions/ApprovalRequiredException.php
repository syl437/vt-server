<?php

namespace App\Exceptions;


use Flugg\Responder\Exceptions\Http\ApiException;

class ApprovalRequiredException extends ApiException
{
    protected $errorCode = 423;
}