<?php

namespace App\Exceptions;


use Flugg\Responder\Exceptions\Http\ApiException;

class InvalidCouponException extends ApiException
{
    protected $errorCode = 424;
}