<?php

namespace App\Transformers;

use App\ApiToken;
use App\Models\User;
use Flugg\Responder\Transformer;

class ApiTokenTransformer extends Transformer
{
    /**
     * A list of all available relations.
     *
     * @var array
     */
    protected $relations = ['*'];

    /**
     * Transform the model data into a generic array.
     *
     * @param  User $user
     * @return array
     */
    public function transform(User $user): array
    {
        return [
            'token' => $user->api_token
        ];
    }
}
