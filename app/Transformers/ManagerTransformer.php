<?php

namespace App\Transformers;

use App\Models\Manager;
use Flugg\Responder\Transformer;

class ManagerTransformer extends Transformer
{
    /**
     * A list of all available relations.
     *
     * @var array
     */
    protected $relations = ['*'];

    /**
     * Transform the model data into a generic array.
     *
     * @param  Manager $manager
     * @return array
     */
    public function transform(Manager $manager):array
    {
        return [
            'id' => (int) $manager->id,
            'token' => $manager->api_token,
            'company_id' => $manager->company_id
        ];
    }
}
