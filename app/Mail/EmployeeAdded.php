<?php

namespace App\Mail;

use App\Models\Company;
use App\Models\Manager;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmployeeAdded extends Mailable
{
    use Queueable, SerializesModels;

    public $email;
    public $password;
    public $company;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email, $password, $company)
    {
        $this->email = $email;
        $this->password = $password;
        $this->company = $company;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.employee_added');
    }
}
