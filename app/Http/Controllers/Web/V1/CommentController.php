<?php

namespace App\Http\Controllers\Web\V1;

use App\Models\Comment;
use App\Models\File;
use App\Http\Controllers\Controller;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(File $file)
    {
        $comments = Comment::where('media_id', $file->id);
        return responder()->success($comments);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(File $file, Comment $comment)
    {
        $comment->delete();
        return responder()->success();
    }

}
