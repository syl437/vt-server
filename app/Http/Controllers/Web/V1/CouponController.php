<?php

namespace App\Http\Controllers\Web\V1;

use App\Models\Company;
use App\Models\Coupon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Company $company)
    {
        app()->setLocale('he');

        $coupons = $company->coupons;

        foreach ($coupons as $coupon) {
            $coupon->load('users');
            $coupon->purchases = $coupon->users->count();
            $coupon->uses = $coupon->users->filter(function($user) {
                return $user->pivot->used;
            })->count();
        }

        return responder()->success($coupons);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Company $company)
    {
        app()->setLocale('he');
        $coupon = new Coupon();
        $coupon->fill($request->all());
        $coupon->company()->associate($company);
        $coupon->save();

        $coupon->translateOrNew('en')->title = $request->get('title_en');
        $coupon->translateOrNew('he')->title = $request->get('title_he');
        $coupon->translateOrNew('en')->description = $request->get('description_en');
        $coupon->translateOrNew('he')->description = $request->get('description_he');
        $coupon->save();

        return responder()->success($coupon);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company, Coupon $coupon)
    {
        $coupon->getTranslationsArray();

        foreach ($coupon->translations as $tr){
            if ($tr->locale === 'en'){
                $coupon->title_en = $tr->title;
                $coupon->description_en = $tr->description;
            }
            if ($tr->locale === 'he'){
                $coupon->title_he = $tr->title;
                $coupon->description_he = $tr->description;
            }
        }

        return responder()->success($coupon);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company, Coupon $coupon)
    {
        app()->setLocale('he');
        $coupon->fill($request->all());
        $coupon->save();
        $coupon->translateOrNew('en')->title = $request->get('title_en');
        $coupon->translateOrNew('he')->title = $request->get('title_he');
        $coupon->translateOrNew('en')->description = $request->get('description_en');
        $coupon->translateOrNew('he')->description = $request->get('description_he');
        $coupon->save();

        return responder()->success();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company, Coupon $coupon)
    {
        $coupon->delete();
        return responder()->success();
    }
}
