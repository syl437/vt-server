<?php

namespace App\Http\Controllers\Web\V1;

use App\Jobs\SendEmployeeCredentialsEmail;
use App\Models\Company;
use App\Models\Manager;
use Giggsey\Locale\Locale;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use libphonenumber\PhoneNumberUtil;

class ManagerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Company $company)
    {
        return responder()->success($company->managers());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Company $company)
    {
        app()->setLocale('he');

        $check_email = Manager::where('email', $request->get('email'))->get();
        if ($check_email->count() > 0){
            return responder()->error('email_already_exists')->respond();
        }

        $check_owner = Manager::where('company_id', $company->id)->where('owner', 1)->get();

        if ($check_owner->count() > 0 && $request->get('owner') === true){
            return responder()->error('already_owner')->respond();
        }

        if ($check_owner->count() === 0 && $request->get('owner') === false){
            return responder()->error('no_owner')->respond();
        }

        $password = str_random(6);

        $manager = new Manager();
        $manager->name = $request->get('name');
        $manager->email = $request->get('email');
        $manager->password = bcrypt($password);
        $manager->company()->associate($company->id);
        $manager->owner = $request->get('owner');
        $manager->save();

        dispatch(new SendEmployeeCredentialsEmail($manager->email, $password, $company->title));

        return responder()->success();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company, Manager $manager)
    {
        return responder()->success($manager);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company, Manager $manager)
    {
        app()->setLocale('he');

        $check_email = Manager::where('email', $request->get('email'))->get();
        if ($check_email->count() > 1){
            return responder()->error('email_already_exists')->respond();
        }

        $check_owner = Manager::where('company_id', $company->id)->where('owner', 1)->get();

        if ($check_owner->count() > 0 && $request->get('owner') === true){
            return responder()->error('already_owner')->respond();
        }

        $manager->name = $request->get('name');
        $manager->email = $request->get('email');
        $manager->owner = $request->get('owner');
        $manager->save();

        return responder()->success();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company, Manager $manager)
    {
        $manager->delete();
        return responder()->success();
    }

    public function resendCredentials(Company $company, Manager $manager)
    {
        $password = str_random(6);
        $manager->password = bcrypt($password);
        $manager->save();

        dispatch(new SendEmployeeCredentialsEmail($manager->email, $password, $company->title));

        return responder()->success();
    }

    public function getCountries(Company $company, Manager $manager)
    {
        $country_codes = PhoneNumberUtil::getInstance()->getSupportedRegions();
        $countries = collect([]);

        foreach ($country_codes as $country_code){
            $countries->push(["code" => $country_code, "name" => Locale::getDisplayRegion('-' . $country_code, 'he')]);
        }

        return ['data' => $countries->sortBy('name')->values()->all()];
    }
}
