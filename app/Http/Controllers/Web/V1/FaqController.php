<?php

namespace App\Http\Controllers\Web\V1;

use App\Models\Faq;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        app()->setLocale('he');
        return responder()->success(Faq::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $faq = new Faq();
        $faq->save();

        $faq->translateOrNew('en')->question = $request->get('question_en');
        $faq->translateOrNew('he')->question = $request->get('question_he');
        $faq->translateOrNew('en')->answer = $request->get('answer_en');
        $faq->translateOrNew('he')->answer = $request->get('answer_he');
        $faq->save();

        return responder()->success();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Faq $faq)
    {

        $faq->getTranslationsArray();

        foreach ($faq->translations as $tr){
            if ($tr->locale === 'en'){
                $faq->question_en = $tr->question;
                $faq->answer_en = $tr->answer;
            }
            if ($tr->locale === 'he'){
                $faq->question_he = $tr->question;
                $faq->answer_he = $tr->answer;
            }
        }

        return responder()->success($faq);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Faq $faq)
    {
        $faq->translateOrNew('en')->question = $request->get('question_en');
        $faq->translateOrNew('he')->question = $request->get('question_he');
        $faq->translateOrNew('en')->answer = $request->get('answer_en');
        $faq->translateOrNew('he')->answer = $request->get('answer_he');
        $faq->save();

        return responder()->success();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Faq $faq)
    {
        $faq->delete();
        return responder()->success();
    }
}
