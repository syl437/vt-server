<?php

namespace App\Http\Controllers\Web\V1;

use App\Models\CompanyCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CompanyCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        app()->setLocale('he');
        return responder()->success(CompanyCategory::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function show(CompanyCategory $companyCategory)
    {
        app()->setLocale('he');

        $sub = $companyCategory->company_subcategories;

        foreach ($sub as $item){
            $item->getTranslationsArray();
            foreach ($item->translations as $tr){
                if ($tr->locale === 'en'){
                    $item->title_en = $tr->title;
                }
                if ($tr->locale === 'he'){
                    $item->title_he = $tr->title;
                }
            }
        }

        return responder()->success($sub);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
