<?php

namespace App\Http\Controllers\Web\V1;

use App\Jobs\CompressVideo;
use App\Jobs\CreateLogoPin;
use App\Jobs\SendPushNotification;
use App\Models\Banner;
use App\Models\Company;
use App\Models\Coupon;
use App\Models\File;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Plank\Mediable\MediaUploaderFacade;

class MediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $company = Company::find($request->get('company_id'));
        $company->loadMedia();
        $company->medias = collect([]);

        foreach ($company->media as $file) {

            if ($file->aggregate_type === 'video' && $file->compressed && $file->pivot->tag !== 'main_video') {

                $file->user_pivot = DB::table('mediables')
                    ->where('media_id', $file->pivot->media_id)
                    ->where('mediable_type', 'App\Models\User')
                    ->first();

                $file->url = $file->getUrl();

                $file->user = User::find($file->user_pivot->mediable_id);

                $thumbnail = File::find($file->id)->loadMedia();

                if ($thumbnail->media->count() > 0) {
                    $file->thumbnail = $thumbnail->media[0]->getUrl();
                } else {
                    $file->thumbnail = '';
                }

                $company->medias->push($file);

            }

        }

        $company->medias = $company->medias->sortByDesc('created_at')->values()->all();

        return responder()->success($company);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        app()->setLocale('he');

        if ($request->files->count() === 0)
            return responder()->error('no_files')->respond();

        \Log::info(json_encode($request->all()));
        $unix = time();

        switch ($request->get('entity_type')){

            case 'company':

                $company = Company::find($request->get('entity_id'));
                $name = "{$request->get('media_key')}_company_{$company->id}_{$unix}";

                if ($request->get('media_key') === 'main_video'){
                    $media = MediaUploaderFacade::fromSource($request->file($request->get('media_key')))->useFilename($name)->toDirectory('original')->upload();
                } else {
                    $media = MediaUploaderFacade::fromSource($request->file($request->get('media_key')))->useFilename($name)->upload();
                }

                $company->syncMedia($media, $request->get('media_key'));

                if ($media->aggregate_type === 'video'){
                    $this->dispatch(new CompressVideo($media, $company, 'main_video', null));
                }

                if ($media->aggregate_type === 'image'){
                    $this->dispatch(new CreateLogoPin($media));
                }

                $company->loadMedia();
                $company->medias = $company->media;

                return responder()->success($company);

                break;

            case 'user':

                if ($request->get('company_id') == '-1' || $request->get('company_id') == '0'){

                    $add = new Company();
                    $add->company_subcategory()->associate($request->get('company_subcategory_id'));
                    $add->region()->associate($request->get('region_id'));
                    $add->address = $request->get('address');
                    $add->country = $request->get('country');
                    $add->lat = $request->get('lat');
                    $add->lng = $request->get('lng');
                    $add->from_user = 1;
                    $add->save();

                    $add->translateOrNew('en')->title = $request->get('title');
                    $add->translateOrNew('he')->title = $request->get('title');
                    $add->translateOrNew('en')->description = '';
                    $add->translateOrNew('he')->description = '';
                    $add->save();

                    $media = MediaUploaderFacade::importPath('media', 'test_main_pictures/icon.png');
                    $add->syncMedia($media, 'logo');
                    $this->dispatch(new CreateLogoPin($media));

                    $company = Company::find($add->id);

                } else {
                    $company = Company::find($request->get('company_id'));
                }

                $user = User::find($request->get('entity_id'));
                $name = "review_company_{$company->id}_user_{$user->id}_{$unix}";

                $media = MediaUploaderFacade::fromSource($request->file($request->get('media_key')))->useFilename($name)->toDirectory('original')->upload();
                $company->attachMedia($media, $request->get('title'));
                $user->attachMedia($media, $request->get('title'));

                if ($media->aggregate_type === 'video'){
                    $this->dispatch(new CompressVideo($media, $company, $request->get('title'), $user));
                }

                return responder()->success();

                break;

            case 'coupon':

                $coupon = Coupon::find($request->get('entity_id'));
                $name = "coupon_coupon_{$coupon->id}_{$unix}";

                $media = MediaUploaderFacade::fromSource($request->file($request->get('media_key')))->useFilename($name)->upload();
                $coupon->syncMedia($media, $request->get('media_key'));

                $coupon->loadMedia();

                return responder()->success();

                break;

            case 'banner':

                $banner = Banner::find($request->get('entity_id'));
                $name = "banner_image_{$banner->id}_{$unix}";

                $media = MediaUploaderFacade::fromSource($request->file($request->get('media_key')))->useFilename($name)->upload();
                $banner->syncMedia($media, $request->get('media_key'));

                $banner->loadMedia();

                return responder()->success();

                break;

            default:

                break;

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(File $file)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        app()->setLocale('he');

        if ($request->files->count() === 0)
            return responder()->error('no_files')->respond();

        \Log::info($request->files->count());
        \Log::info(json_encode($request->all()));

        $unix = time();

        switch ($request->get('entity_type')){

            case 'company':

                $company = Company::find($request->get('entity_id'));
                $name = "{$request->get('media_key')}_company_{$company->id}_{$unix}";

                if ($request->get('media_key') === 'main_video'){
                    $media = MediaUploaderFacade::fromSource($request->file($request->get('media_key')))->useFilename($name)->toDirectory('original')->upload();
                } else {
                    $media = MediaUploaderFacade::fromSource($request->file($request->get('media_key')))->useFilename($name)->upload();
                }

                $company->syncMedia($media, $request->get('media_key'));

                if ($media->aggregate_type === 'video'){
                    $this->dispatch(new CompressVideo($media, $company, 'main_video', null));
                }

                if ($media->aggregate_type === 'image'){
                    $this->dispatch(new CreateLogoPin($media));
                }

                $company->loadMedia();
                $company->medias = $company->media;

                return responder()->success($company);

                break;

            case 'coupon':

                $coupon = Coupon::find($request->get('entity_id'));
                $name = "coupon_coupon_{$coupon->id}_{$unix}";

                $media = MediaUploaderFacade::fromSource($request->file($request->get('media_key')))->useFilename($name)->upload();
                $coupon->syncMedia($media, $request->get('media_key'));

                $coupon->loadMedia();

                return responder()->success($coupon);

                break;

            case 'banner':

                $banner = Banner::find($request->get('entity_id'));
                $name = "banner_image_{$banner->id}_{$unix}";

                $media = MediaUploaderFacade::fromSource($request->file($request->get('media_key')))->useFilename($name)->upload();
                $banner->syncMedia($media, $request->get('media_key'));

                $banner->loadMedia();

                return responder()->success($banner);

                break;

            case 'default':

                break;

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(File $file)
    {
        \DB::table('mediables')->where('media_id', $file->id)->delete();
        $file->delete();
        return responder()->success();
    }

    public function changeStatus(Request $request, File $file)
    {
        $file->status = $request->get('status');
        $file->save();

        if ($request->get('status') === 'approved'){
            $user = User::find($request->get('user_id'));
            $user->points += 3;
            $user->save();

            $schedule = Carbon::now();
            if ($user->language === 'en'){
                $text = 'Well done. Your movie has been successfully approved. Your account is credited in 3 Y$';
            } else {
                $text = 'כל הכבוד. הסרט שלך אושר בהצלחה .חשבונך זוכה בעוד 3 Y$';
            }
            dispatch(new SendPushNotification($user, $text, $schedule->format('Y-m-d H:i:s'), ['topic' => 'points']));

        }

        return responder()->success();
    }
}
