<?php

namespace App\Http\Controllers\Web\V1;

use App\Jobs\SendPushNotification;
use App\Models\User;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return responder()->success(User::all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return responder()->success();
    }

    public function sendPoints(Request $request, User $user)
    {
        $user->points += $request->get('points');
        $user->save();

        $schedule = Carbon::now();

        if ($user->language === 'en'){
            $text = 'Your account is credited in '. $request->get('points') . ' Y$';
        } else {
            $text = 'חשבונך זוכה בעוד '. $request->get('points') .' Y$';
        }
        dispatch(new SendPushNotification($user, $text, $schedule->format('Y-m-d H:i:s'), ['topic' => 'points']));

        return responder()->success();
    }

    public function sendPush(Request $request)
    {

        if ($request->get('user_id') != 0){
            $user = User::find($request->get('user_id'));
            dispatch(new SendPushNotification($user, $request->get('text'), null, null));
        } else {
            $users = User::where('push_id', '!=', NULL)->get();
            foreach ($users as $user){
                dispatch(new SendPushNotification($user, $request->get('text'), null, null));
            }
        }

        return responder()->success();
    }
}
