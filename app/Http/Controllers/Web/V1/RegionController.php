<?php

namespace App\Http\Controllers\Web\V1;

use App\Models\Region;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        app()->setLocale('he');

        $regions = Region::all();

        foreach ($regions as $region){
            $region->getTranslationsArray();
            foreach ($region->translations as $tr){
                if ($tr->locale === 'en'){
                    $region->title_en = $tr->title;
                }
                if ($tr->locale === 'he'){
                    $region->title_he = $tr->title;
                }
            }
        }

        return responder()->success($regions);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        app()->setLocale('he');
        $region = new Region();
        $region->save();

        $region->translateOrNew('en')->title = $request->get('title_en');
        $region->translateOrNew('he')->title = $request->get('title_he');
        $region->save();

        return responder()->success();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Region $region)
    {
        app()->setLocale(request('lang'));
        $region->translateOrNew('en')->title = $request->get('title_en');
        $region->translateOrNew('he')->title = $request->get('title_he');
        $region->save();

        return responder()->success();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Region $region)
    {
        $region->delete();
        return responder()->success();
    }
}
