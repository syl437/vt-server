<?php

namespace App\Http\Controllers\Web\V1;

use App\Models\CompanySubcategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CompanySubcategoryController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        app()->setLocale(request('lang'));
        $sub = new CompanySubcategory();
        $sub->company_category()->associate($request->get('company_category_id'));
        $sub->save();

        $sub->translateOrNew('en')->title = $request->get('title_en');
        $sub->translateOrNew('he')->title = $request->get('title_he');
        $sub->save();

        return responder()->success();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CompanySubcategory $companySubcategory)
    {
        app()->setLocale('he');
        $companySubcategory->translateOrNew('en')->title = $request->get('title_en');
        $companySubcategory->translateOrNew('he')->title = $request->get('title_he');
        $companySubcategory->save();

        return responder()->success();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompanySubcategory $companySubcategory)
    {
        $companySubcategory->delete();
        return responder()->success();
    }
}
