<?php

namespace App\Http\Controllers\Web\V1;

use App\Models\Admin;
use App\Models\Manager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TokenController extends Controller
{
    public function login(Request $request)
    {

        $manager = Manager::whereEmail($request->get('login'))->first();

        if ($manager){

            if (!\Hash::check($request->get('password'), $manager->password)){
                return responder()->error('invalid_credentials')->respond();
            }

            return responder()->success($manager, function ($manager) {
                return ['id' => $manager->id, 'company_id' => $manager->company_id];
            })->respond();

        }

        $admin = Admin::where('username', $request->get('login'))->first();

        if ($admin){
            if (!\Hash::check($request->get('password'), $admin->password)){
                return responder()->error('invalid_credentials')->respond();
            }

            return responder()->success($admin, function ($admin) {
                return ['id' => $admin->id];
            })->respond();
        }

        return responder()->error('invalid_credentials')->respond();
    }
}
