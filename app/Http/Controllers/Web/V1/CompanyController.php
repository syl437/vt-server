<?php

namespace App\Http\Controllers\Web\V1;

use App\Http\Requests\CreateCompany;
use App\Jobs\SendNewCompanyNotificationEmail;
use App\Models\Company;
use App\Models\CompanyCategory;
use App\Models\File;
use App\Models\Manager;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use libphonenumber\PhoneNumberFormat;

class CompanyController extends Controller
{
    public function index()
    {
        app()->setLocale('he');
        $companies = Company::all();
        return responder()->success($companies);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        app()->setLocale('he');

        $company = new Company();
        $company->fill($request->all());
        $company->phone = phone($request->get('phone'), $country_code = $request->get('country'), $format = PhoneNumberFormat::E164);
        $company->company_subcategory()->associate($request->get('company_subcategory_id'));
        $company->region()->associate($request->get('region_id'));
        $company->approved = 1;
        $company->save();

        $company->translateOrNew('en')->title = $request->get('title_en');
        $company->translateOrNew('he')->title = $request->get('title_he');
        $company->translateOrNew('en')->description = $request->get('description_en');
        $company->translateOrNew('he')->description = $request->get('description_he');
        $company->translateOrNew('en')->facebook = 'Sharing video from YTravel application. Welcome to ' . $request->get('title_en') . '!';
        $company->translateOrNew('he')->facebook = 'שיתוף סרט מאפליקציית Ytravel. ברוכים הבאים ל' . $request->get('title_he') . '!';
        $company->translateOrNew('en')->sms = 'Welcome to YTravel!';
        $company->translateOrNew('he')->sms = 'ברוכים הבאים לYTravel!';
        $company->save();

        dispatch(new SendNewCompanyNotificationEmail($company));

        return responder()->success($company);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        app()->setLocale('he');

        $company->getTranslationsArray();
        foreach ($company->translations as $tr){
            if ($tr->locale === 'en'){
                $company->title_en = $tr->title;
                $company->description_en = $tr->description;
            }
            if ($tr->locale === 'he'){
                $company->title_he = $tr->title;
                $company->description_he = $tr->description;
            }
        }

        return responder()->success($company);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {

        app()->setLocale('he');

        if ($request->get('phone')){

            $company->fill($request->all());
            $company->phone = phone($request->get('phone'), $country_code = $request->get('country'), $format = PhoneNumberFormat::E164);
            $company->company_subcategory()->associate($request->get('company_subcategory_id'));
            $company->region()->associate($request->get('region_id'));
            $company->save();

        }

        return responder()->success($company);

    }

    public function destroy(Company $company)
    {
        $all_files = DB::table('mediables')->where('mediable_type', 'App\Models\Company')->where('mediable_id', $company->id)->get();

        foreach ($all_files as $file){
            DB::table('mediables')->where('media_id', $file->media_id)->delete();
        }

        $company->delete();
        return responder()->success();
    }

    public function getPurchases(Company $company)
    {
        $coupons = $company->coupons;
        $purchases = [];

        foreach ($coupons as $coupon){
            $items = DB::table('coupon_user')->where('coupon_id', $coupon->id)->get();

            if ($items->count()){
                foreach ($items as $item){
                    $item->coupon = $coupon;
                    $item->user = User::find($item->user_id);
                    $purchases[] = $item;
                }
            }
        }

        return ["data" => $purchases];
    }

    public function approveCompany(Company $company)
    {
        $company->approved = 1;
        $company->save();

        return responder()->success();
    }

    public function suspendCompany(Company $company)
    {
        $company->approved = 0;
        $company->save();

        return responder()->success();
    }
}
