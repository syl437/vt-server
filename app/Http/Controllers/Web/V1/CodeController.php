<?php

namespace App\Http\Controllers\Web\V1;

use Aira\Promocodes\Model\Promocode;
use App\Jobs\SendSMS;
use App\Models\Company;
use App\Models\Manager;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use libphonenumber\PhoneNumberFormat;
use Promocodes;

class CodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Company $company)
    {
        $manager_ids = $company->managers()->pluck('id');
        $codes = Promocode::whereIn('label', $manager_ids)->get();

        foreach ($codes as $code){
            if ($code->rewardable_id){
                $code->recipient = User::find($code->rewardable_id);
            }
            if ($code->label){
                $code->sender = Manager::find($code->label);
            }
        }

        return responder()->success($codes);
    }

    public function store(Request $request)
    {
        $manager_id = $request->get('manager_id');
        $quantity = $request->get('quantity');
        $language = $request->get('language');

        try {
            $phone = phone($request->get('phone'), $country_code = $request->get('country'), $format = PhoneNumberFormat::E164);
        } catch (\Exception $e) {
            return responder()->error('invalid_phone')->respond();
        }

        $company = Manager::find($manager_id)->company;
        $text = $company->getTranslation($language)->sms;

        $code = Promocodes::create($manager_id, 1, $quantity);

        if ($language == 'en'){
            $msg = $text . ' For details click here: https://app.y-travel.net/landing?code=' . $code[0]['code'] . '&points=' . $quantity . '&lang=' . $language . '&company=' . $company->id;
        } else {
            $msg = $text . ' לפרטים לחץ כאן: ' . 'https://app.y-travel.net/landing?code=' . $code[0]['code'] . '&points=' . $quantity . '&lang=' . $language . '&company=' . $company->id;

        }

        dispatch(new SendSMS($msg, $phone, $request->get('country')));

        return ["data" => ['phone' => $phone]];
    }

}
