<?php

namespace App\Http\Controllers\Web\V1;

use App\Models\Company;
use App\Models\File;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LandingController extends Controller
{
    public function index(Request $request)
    {
        $lang = $request->get('lang');
        app()->setLocale($lang);

        $code = $request->get('code');
        $points = $request->get('points');
        $company = Company::find($request->get('company'));
        return view('landing', ['code' => $code, 'points' => $points, 'lang' => $lang, 'name' => $company->title]);
    }

    public function show(Request $request)
    {
        $id = $request->get('id');
        $type = $request->get('type');
        $lang = $request->get('lang');
        $dir = $lang == 'en' ? 'ltr' : 'rtl';
        app()->setLocale($lang);

        if ($type == 'user'){

            // user video

            $connections = \DB::table('mediables')->where('media_id', $id)->get();

            foreach ($connections as $connection){
                if ($connection->mediable_type == 'App\Models\Company'){
                    $company = Company::find($connection->mediable_id);
                }
                if ($connection->mediable_type == 'App\Models\User'){
                    $user = User::find($connection->mediable_id);
                }
            }

            $description = $connections[0]->tag;
            $url = $user->getMedia($description)[0]->getUrl();

            $thumbnail = File::find($user->getMedia($description)[0]->id)->loadMedia();

            if ($thumbnail->media->count() > 0){
                $image = $thumbnail->media[0]->getUrl();
            } else {
                $image = resource_path('images/icon.png');
            }

            $title = $company->title;

            $logo = $user->getMedia('avatar');

            if ($logo->count()){
                $logo = $user->getMedia('avatar')[0]->getUrl();
            } else {
                $logo = resource_path('images/icon.png');
            }

        } else if ($type == 'company'){

            // company main video

            $company = Company::find($id);
            $url = $company->getMedia('main_video')[0]->getUrl();
            $title = $company->title;
            $description = $company->facebook;

            $thumbnail = File::find($company->getMedia('main_video')[0]->id)->loadMedia();

            if ($thumbnail->media->count() > 0){
                $image = $thumbnail->media[0]->getUrl();
            } else {
                $image = resource_path('images/icon.png');
            }

            $logo = $company->getMedia('logo');

            if ($logo->count()){
                $logo = $company->getMedia('logo')[0]->getUrl();
            } else {
                $logo = resource_path('images/icon.png');
            }

        }

        return view('video', ['url' => $url, 'title' => $title, 'description' => $description, 'image' => $image, 'dir' => $dir, 'logo' => $logo]);
    }
}
