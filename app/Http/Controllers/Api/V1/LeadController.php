<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\CreateLead;
use App\Http\Controllers\Controller;
use App\Models\Lead;

class LeadController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateLead $request)
    {
        $lead = new Lead();
        $lead->fill($request->all());
        $lead->user()->associate($request->get('user_id'));
        $lead->company()->associate($request->get('company_id'));
        $lead->save();

        return responder()->success();
    }

}
