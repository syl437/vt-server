<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\CompanyCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CompanyCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(CompanyCategory $companyCategory)
    {
        app()->setLocale(request('lang'));
        return responder()->success($companyCategory::all());
    }

}
