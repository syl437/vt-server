<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\CreateManager;
use App\Jobs\SendEmployeeCredentialsEmail;
use App\Mail\EmployeeAdded;
use App\Models\Company;
use App\Models\Manager;
use Flugg\Responder\Exceptions\Http\UnauthenticatedException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class ManagerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateManager $request)
    {
        $company = Company::find($request->get('company_id'));

        $password = str_random(6);

        $manager = new Manager();
        $manager->name = $request->get('name');
        $manager->email = $request->get('email');
        $manager->password = bcrypt($password);
        $manager->company()->associate($company->id);
        $manager->owner = 0;
        $manager->save();

        dispatch(new SendEmployeeCredentialsEmail($manager->email, $password, $company->title));

        $company->refresh();

        return responder()->success($company->managers());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Manager $manager)
    {
        return responder()->success($manager);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Manager $manager)
    {

        if ($request->has('name')){

            $manager->name = $request->get('name');
            $manager->save();

        }

        if ($request->has('old_password')){

            if (!Hash::check($request->get('old_password'), $manager->password))
                return responder()->error('invalid_old_password')->respond();

            else {

                $manager->password = bcrypt($request->get('new_password'));
                $manager->save();

            }

        }

        return responder()->success($manager);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Manager $manager)
    {
        $company = Company::find($manager->company_id);

        $manager->delete();

        $company->refresh();

        return responder()->success($company->managers());
    }
}
