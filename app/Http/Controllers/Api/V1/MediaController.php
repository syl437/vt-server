<?php

namespace App\Http\Controllers\Api\V1;

use App\Jobs\CompressVideo;
use App\Jobs\CreateLogoPin;
use App\Jobs\ExtractVideoThumbnail;
use App\Jobs\SendVideoNotification;
use App\Models\Company;
use App\Models\Coupon;
use App\Models\File;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Plank\Mediable\MediaUploaderFacade;


class MediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        app()->setLocale(request('lang'));

        if ($request->files->count() === 0)
            return responder()->error('no_files')->respond();

        \Log::info(json_encode($request->all()));
        $unix = time();

        switch ($request->get('entity_type')){

            case 'company':

                $company = Company::find($request->get('entity_id'));
                $name = "{$request->get('media_key')}_company_{$company->id}_{$unix}";

                if ($request->get('media_key') === 'main_video'){
                    $media = MediaUploaderFacade::fromSource($request->file($request->get('media_key')))->useFilename($name)->toDirectory('original')->upload();
                } else {
                    $media = MediaUploaderFacade::fromSource($request->file($request->get('media_key')))->useFilename($name)->upload();
                }

                $company->syncMedia($media, $request->get('media_key'));

                if ($media->aggregate_type === 'video'){
                    $this->dispatch(new CompressVideo($media, $company, 'main_video', null));
                }

                if ($media->aggregate_type === 'image'){
                    $this->dispatch(new CreateLogoPin($media));
                }

                $company->loadMedia();
                $company->medias = $company->media;

                return responder()->success($company);

                break;

            case 'user':

                if ($request->get('company_id') == '-1' || $request->get('company_id') == '0'){

                    $add = new Company();
                    $add->company_subcategory()->associate($request->get('company_subcategory_id'));
                    $add->region()->associate($request->get('region_id'));
                    $add->address = $request->get('address');
                    $add->country = $request->get('country');
                    $add->lat = $request->get('lat');
                    $add->lng = $request->get('lng');
                    $add->from_user = 1;
                    $add->approved = 0;
                    $add->save();

                    $add->translateOrNew('en')->title = $request->get('place');
                    $add->translateOrNew('he')->title = $request->get('place');
                    $add->translateOrNew('en')->description = '';
                    $add->translateOrNew('he')->description = '';
                    $add->translateOrNew('en')->facebook = 'Sharing video from YTravel application. Welcome to ' . $request->get('place') . '!';
                    $add->translateOrNew('he')->facebook = 'שיתוף סרט מאפליקציית Ytravel. ברוכים הבאים ל' . $request->get('place') . '!';
                    $add->translateOrNew('en')->sms = 'Welcome to YTravel!';
                    $add->translateOrNew('he')->sms = 'ברוכים הבאים לYTravel!';
                    $add->save();

                    $logo_name = "logo_company_{$add->id}_{$unix}";
                    $media = MediaUploaderFacade::fromSource(public_path('/media/test_main_pictures/icon.png'))->useFilename($logo_name)->upload();
                    $add->syncMedia($media, 'logo');
                    $this->dispatch(new CreateLogoPin($media));

                    $company = Company::find($add->id);

                } else {
                    $company = Company::find($request->get('company_id'));
                }

                $user = User::find($request->get('entity_id'));
                $name = "review_company_{$company->id}_user_{$user->id}_{$unix}";

                $media = MediaUploaderFacade::fromSource($request->file($request->get('media_key')))->useFilename($name)->toDirectory('original')->upload();

                $title = $request->get('title');
                if (is_numeric($title)){
                    $title .= '.';
                }

                $company->attachMedia($media, $title);
                $user->attachMedia($media, $title);

                if ($media->aggregate_type === 'video'){
                    $this->dispatch(new CompressVideo($media, $company, $title, $user));
                    dispatch(new SendVideoNotification($company->id, $user));
                }

                return responder()->success();

                break;

            case 'avatar':

                $user = User::find($request->get('entity_id'));
                $name = "avatar_user_{$user->id}_{$unix}";

                $media = MediaUploaderFacade::fromSource($request->file($request->get('media_key')))->useFilename($name)->upload();
                $user->syncMedia($media, $request->get('media_key'));

                $user->loadMedia();

                $user->medias = $user->media;

                return responder()->success($user);

                break;

            case 'coupon':

                $coupon = Coupon::find($request->get('entity_id'));
                $name = "coupon_coupon_{$coupon->id}_{$unix}";

                $media = MediaUploaderFacade::fromSource($request->file($request->get('media_key')))->useFilename($name)->upload();
                $coupon->syncMedia($media, $request->get('media_key'));

                $coupon->loadMedia();

                return responder()->success();

                break;

            default:

                break;

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        app()->setLocale(request('lang'));

        if ($request->files->count() === 0)
            return responder()->error('no_files')->respond();

        \Log::info($request->files->count());
        \Log::info(json_encode($request->all()));

        $unix = time();

        switch ($request->get('entity_type')){

            case 'company':

                $company = Company::find($request->get('entity_id'));
                $name = "{$request->get('media_key')}_company_{$company->id}_{$unix}";

                if ($request->get('media_key') === 'main_video'){
                    $media = MediaUploaderFacade::fromSource($request->file($request->get('media_key')))->useFilename($name)->toDirectory('original')->upload();
                } else {
                    $media = MediaUploaderFacade::fromSource($request->file($request->get('media_key')))->useFilename($name)->upload();
                }

                $company->syncMedia($media, $request->get('media_key'));

                if ($media->aggregate_type === 'video'){
                    $this->dispatch(new CompressVideo($media, $company, 'main_video', null));
                }

                if ($media->aggregate_type === 'image'){
                    $this->dispatch(new CreateLogoPin($media));
                }

                $company->loadMedia();
                $company->medias = $company->media;

                return responder()->success($company);

                break;

            case 'avatar':

                $user = User::find($request->get('entity_id'));
                $name = "avatar_user_{$user->id}_{$unix}";

                $media = MediaUploaderFacade::fromSource($request->file($request->get('media_key')))->useFilename($name)->upload();
                $user->syncMedia($media, $request->get('media_key'));

                $user->loadMedia();

                $user->medias = $user->media;

                return responder()->success($user);

                break;

            case 'coupon':

                $coupon = Coupon::find($request->get('entity_id'));
                $name = "coupon_coupon_{$coupon->id}_{$unix}";

                $media = MediaUploaderFacade::fromSource($request->file($request->get('media_key')))->useFilename($name)->upload();
                $coupon->syncMedia($media, $request->get('media_key'));

                $coupon->loadMedia();

                return responder()->success($coupon);

                break;

            case 'default':

                break;

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(File $file)
    {

        $user = auth()->guard('api')->user();

        if ($file->status === 'approved' && $user->points >= 3){
            $user->points = $user->points - 3;
            $user->save();
        }

        \DB::table('mediables')->where('media_id', $file->id)->delete();

        $file->delete();

        return responder()->success();
    }
}
