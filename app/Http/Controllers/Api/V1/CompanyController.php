<?php

namespace App\Http\Controllers\Api\V1;

use Aira\Promocodes\Model\Promocode;
use App\Http\Requests\CreateCompany;
use App\Jobs\SendNewCompanyNotificationEmail;
use App\Models\Company;
use App\Models\CompanyCategory;
use App\Models\File;
use App\Models\Manager;
use App\Models\Region;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use libphonenumber\PhoneNumberFormat;
use Plank\Mediable\Media;

class CompanyController extends Controller
{
    public function index()
    {
        app()->setLocale(request('lang'));

        $company_category_id = request('company_category_id');
        $region_id = request('region_id');

        if ($company_category_id === '0' && $region_id === '0')
            $companies = Company::where('approved', 1)->get();
        else if ($company_category_id === '0' && $region_id !== '0')
            $companies = Company::whereRegionId($region_id)->where('approved', 1)->get();
        else if ($company_category_id !== '0' && $region_id === '0')
            $companies = CompanyCategory::find($company_category_id)->companies()->where('approved', 1)->get();
        else
            $companies = CompanyCategory::find($company_category_id)->companies()->whereRegionId($region_id)->where('approved', 1)->get();

        return responder()->success($companies);
    }

    public function getUserReviews()
    {
        app()->setLocale(request('lang'));

        $subcategories = CompanyCategory::find(request('company_category_id'))->company_subcategories()->pluck('id');
        $region_id = request('region_id');
        $user = auth()->guard('api')->user();

        if ($region_id == 0){
            $regions = Region::all()->pluck('id');
        } else {
            $regions = [$region_id];
        }

        $all = \DB::table('mediables')
            ->join('media', 'mediables.media_id', '=', 'media.id')
            ->join('companies', 'mediables.mediable_id', '=', 'companies.id')
            ->where('mediables.mediable_type', 'App\Models\Company')
            ->where('mediables.tag', '!=', 'main_video')
            ->where('media.aggregate_type', 'video')
            ->where('media.compressed', '1')
            ->whereIn('companies.region_id', $regions)
            ->whereIn('companies.company_subcategory_id', $subcategories)
            ->orderByDesc('media.created_at')
            ->paginate(20);

        $user_reviews = collect([]);

        foreach ($all as $item) {

            $file = Media::find($item->media_id);

            $file->url = $file->getUrl();
            $file->title = $item->tag;

            $user_pivot = DB::table('mediables')
                ->where('media_id', $item->media_id)
                ->where('mediable_type', 'App\Models\User')
                ->where('tag', $item->tag)
                ->first();

            if ($user_pivot){
                $file->user = User::find($user_pivot->mediable_id);
            }

            if ($file->user){
                if ($file->user->isFollowedBy($user))
                    $file->user->followed = 1;
                else
                    $file->user->followed = 0;
            }

            $thumbnail = File::find($file->id)->loadMedia();

            if ($thumbnail->media->count() > 0) {
                $file->thumbnail = $thumbnail->media[0]->getUrl();
            } else {
                $file->thumbnail = '';
            }

            if ($item){
                $company = Company::find($item->mediable_id);
            }

            if ($company){
                $file->company_category = $company->company_subcategory->company_category->title;
                $file->company_subcategory = $company->company_subcategory->title;
                $file->region = $company->region->title;
            }

            $user_reviews->push($file);

        }

        return ['data' => ['info' => $all, 'user_reviews' => $user_reviews]];

    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCompany $request)
    {
        app()->setLocale(request('lang'));

        $company = new Company();
        $company->fill($request->all());
        $company->phone = phone($request->get('phone'), $country_code = $request->get('country'), $format = PhoneNumberFormat::E164);
        $company->company_subcategory()->associate($request->get('company_subcategory_id'));
        $company->region()->associate($request->get('region_id'));
        $company->save();

        $company->translateOrNew('en')->title = $request->get('title_en');
        $company->translateOrNew('he')->title = $request->get('title_he');
        $company->translateOrNew('en')->description = $request->get('description_en');
        $company->translateOrNew('he')->description = $request->get('description_he');
        $company->translateOrNew('en')->facebook = 'Sharing video from YTravel application. Welcome to ' . $request->get('title_en') . '!';
        $company->translateOrNew('he')->facebook = 'שיתוף סרט מאפליקציית Ytravel. ברוכים הבאים ל' . $request->get('title_he') . '!';
        $company->translateOrNew('en')->sms = 'Welcome to YTravel!';
        $company->translateOrNew('he')->sms = 'ברוכים הבאים לYTravel!';
        $company->save();

        $manager = new Manager();
        $manager->email = $request->get('email');
        $manager->password = bcrypt($request->get('password'));
        $manager->owner = 1;
        $manager->company()->associate($company->id);
        $manager->save();

        $company->load('managers');

        dispatch(new SendNewCompanyNotificationEmail($company));

        return responder()->success($company);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        app()->setLocale(request('lang'));

        $company->loadMedia();
        $company->videos = collect([]);
        $company->medias = $company->media;

        foreach ($company->media as $file){

            $file->url = $file->getUrl();

            if ($file->aggregate_type === 'video' && $file->compressed && $file->pivot->tag !== 'main_video'){

                $file->date = $file->created_at->format('Y-m-d');
                $user = DB::table('mediables')->where('tag', $file->pivot->tag)->where('media_id', $file->pivot->media_id)->where('mediable_type', 'App\Models\User')->first();
                if ($user){
                    $file->user = User::find($user->mediable_id);
                }

                $thumbnail = File::find($file->id)->loadMedia();

                if ($thumbnail->media->count() > 0){
                    $file->thumbnail = $thumbnail->media[0]->getUrl();
                } else {
                    $file->thumbnail = '';
                }

                $company->videos->push($file);
            }

        }

        $company->load('managers', 'coupons');

        return responder()->success($company);
    }

    public function showForAdmin(Company $company)
    {
        app()->setLocale('he');

        $company->getTranslationsArray();
        foreach ($company->translations as $tr){
            if ($tr->locale === 'en'){
                $company->title_en = $tr->title;
                $company->description_en = $tr->description;
                $company->facebook_en = $tr->facebook;
                $company->sms_en = $tr->sms;
            }
            if ($tr->locale === 'he'){
                $company->title_he = $tr->title;
                $company->description_he = $tr->description;
                $company->facebook_he = $tr->facebook;
                $company->sms_he = $tr->sms;
            }
        }

        return responder()->success($company);
    }

    public function updateForAdmin(Request $request, Company $company)
    {
        $company->translate('en')->facebook = $request->get('facebook_en');
        $company->translate('he')->facebook = $request->get('facebook_he');
        $company->translate('en')->sms = $request->get('sms_en');
        $company->translate('he')->sms = $request->get('sms_he');
        $company->save();

        return responder()->success();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {

        app()->setLocale(request('lang'));

        if ($request->get('phone')){

            $company->translate('en')->title = $request->get('title_en');
            $company->translate('he')->title = $request->get('title_he');
            $company->translate('en')->description = $request->get('description_en');
            $company->translate('he')->description = $request->get('description_he');
            $company->address = $request->get('address');
            $company->phone = phone($request->get('phone'), $country_code = $request->get('country'), $format = PhoneNumberFormat::E164);
            $company->company_subcategory()->associate($request->get('company_subcategory_id'));
            $company->region()->associate($request->get('region_id'));
            $company->save();

        }

        $company->load('managers', 'coupons');

        return responder()->success($company);

    }

    public function sortBySubcategoryAndRegion()
    {
        app()->setLocale(request('lang'));

        $company_subcategory_id = request('company_subcategory_id');
        $region_id = request('region_id');

        if (!$company_subcategory_id && !$region_id) {
            $companies = Company::where('approved', 1)->get();
        } else {
            $companies = Company::whereRegionId($region_id)->where('company_subcategory_id', $company_subcategory_id)->where('approved', 1)->get();
        }

        return responder()->success($companies);
    }

    public function getStatistics(Company $company)
    {
        $manager_ids = $company->managers()->pluck('id');
        $codes = Promocode::whereIn('label', $manager_ids)->get();

        foreach ($codes as $code){
            if ($code->rewardable_id){
                $code->recipient = User::find($code->rewardable_id);
            }
            if ($code->label){
                $code->sender = Manager::find($code->label);
            }
        }

        $coupons = $company->coupons;
        $purchases = [];

        foreach ($coupons as $coupon){
            $items = DB::table('coupon_user')->where('coupon_id', $coupon->id)->get();

            if ($items->count()){
                foreach ($items as $item){
                    $item->coupon = $coupon;
                    $item->user = User::find($item->user_id);
                    $purchases[] = $item;
                }
            }
        }

        return ["data" => ['codes' => $codes, 'purchases' => $purchases]];
    }
}
