<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Admin;
use App\Models\Banner;
use App\Models\Company;
use App\Models\CompanyCategory;
use App\Models\Faq;
use App\Models\Point;
use App\Models\Region;
use App\Http\Controllers\Controller;
use App\Models\User;
use Giggsey\Locale\Locale;
use libphonenumber\PhoneNumberUtil;
use Propaganistas\LaravelPhone\LaravelPhoneFacade;
use Propaganistas\LaravelPhone\LaravelPhoneServiceProvider;

class InitializationController extends Controller
{

    public function index()
    {
        app()->setLocale(request('lang'));

        $data = [];

        $user = auth()->guard('api')->user();

        $data['regions'] = Region::all();
        $data['company_categories'] = CompanyCategory::all();
        $data['user'] = $user;
        $data['phone'] = Admin::first()->phone;
        $data['following'] = $user->getFollowingList();
        $data['faq'] = Faq::all();
        $data['banners'] = Banner::where('language', request('lang'))->orWhereNull('language')->get();

        return ['data' => $data];

    }

    public function show($id)
    {
        app()->setLocale(request('lang'));

        $data = [];

        $data['company_categories'] = CompanyCategory::all();
        $data['regions'] = Region::all();
        $company = Company::find($id);
        $company->loadMedia();
        $company->medias = $company->media;
        $company->load('managers', 'coupons');
        $data['company'] = $company;
        $data['points'] = Point::all();

        return ['data' => $data];

    }

    public function getCountries()
    {
        $country_codes = PhoneNumberUtil::getInstance()->getSupportedRegions();
        $countries = collect([]);

        foreach ($country_codes as $country_code){
            $countries->push(["code" => $country_code, "name" => Locale::getDisplayRegion('-' . $country_code, request('lang'))]);
        }

        return ['data' => $countries->sortBy('name')->values()->all()];
    }

    public function makeStandard()
    {
        $en = \DB::table('company_translations')->where('locale', 'en')->get();

        foreach ($en as $item_en) {

            \DB::table('company_translations')->where('id', $item_en->id)->update([
                'facebook' => 'Sharing video from YTravel application. Welcome to ' . $item_en->title . '!',
                'sms' => 'Welcome to YTravel!'
            ]);

        }

        $he = \DB::table('company_translations')->where('locale', 'he')->get();

        foreach ($he as $item_he) {

            \DB::table('company_translations')->where('id', $item_he->id)->update([
                'facebook' => 'שיתוף סרט מאפליקציית Ytravel. ברוכים הבאים ל' . $item_he->title . '!',
                'sms' => 'ברוכים הבאים לYTravel!'
            ]);

        }

        return 'OK';
    }

}
