<?php

namespace App\Http\Controllers\Api\V1;

use App\Exceptions\RatingExistsException;
use App\Http\Requests\CreateRate;
use App\Models\Company;
use App\Models\Rate;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRate $request)
    {
        $rate = new Rate();
        $rate->mark = $request->get('mark');
        $rate->user()->associate($request->get('user_id'));
        $rate->company()->associate($request->get('company_id'));
        $rate->save();

        $company = Company::find($request->get('company_id'));

        return responder()->success($company);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function check(User $user, Company $company)
    {
        app()->setLocale(request('lang'));

        $check = Rate::where('user_id', $user->id)->where('company_id', $company->id)->get();

        if ($check->count() > 0)
            return responder()->error('already_rated')->respond();

        return responder()->success();
    }
}
