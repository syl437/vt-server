<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Comment;
use App\Models\Company;
use App\Models\CompanyCategory;
use App\Models\CompanySubcategory;
use App\Models\File;
use App\Models\Region;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Plank\Mediable\Media;

class FollowerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        app()->setLocale(request('lang'));

        $user = auth()->guard('api')->user();

        $all = \DB::table('mediables')
            ->join('media', 'mediables.media_id', '=', 'media.id')
            ->where('mediables.mediable_type', 'App\Models\User')
            ->where('mediables.tag', '!=', 'avatar')
            ->orderByDesc('media.created_at')
            ->paginate(20);
//            ->get();

        $videos = collect([]);

        foreach ($all as $item) {

            $file = Media::find($item->media_id);

            if ($file->status !== 'rejected' && $file->compressed){

                $file->url = $file->getUrl();

                $file->user = User::find($item->mediable_id);

                if ($file->user && $file->user->isFollowedBy($user))
                    $file->user->followed = 1;
                else
                    $file->user->followed = 0;

                $thumbnail = File::find($file->id)->loadMedia();

                if ($thumbnail->media->count() > 0){
                    $file->thumbnail = $thumbnail->media[0]->getUrl();
                } else {
                    $file->thumbnail = '';
                }

                $file->comments = Comment::where('media_id', $file->id)->get();

                $file->title = $item->tag;

                $company = \DB::table('mediables')
                    ->where('media_id', $file->id)
                    ->where('mediable_type', 'App\Models\Company')
                    ->where('tag', $file->title)
                    ->first();

                if ($company){
                    $company = Company::find($company->mediable_id);
                    $file->company_category = CompanyCategory::find($company->company_category_id)->title;
                    $file->company_subcategory = CompanySubcategory::find($company->company_subcategory_id)->title;
                    $file->region = Region::find($company->region_id)->title;
                }

                $videos->push($file);

            }
        }

        $videos = $videos->sortByDesc('created_at')->values()->all();

        return ['data' => ['info' => $all, 'videos' => $videos]];

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth()->guard('api')->user();
        $recipient = User::find($request->get('recipient_id'));
        $user->follow($recipient);
        $recipient->acceptFollowRequestFrom($user);

        $users = $user->getFollowingList();

        return responder()->success($users);
    }

    public function show($id)
    {
        app()->setLocale(request('lang'));

        $requester = auth()->guard('api')->user();

        $all = \DB::table('mediables')
            ->join('followers', 'mediables.mediable_id', '=', 'followers.recipient_id')
            ->join('media', 'mediables.media_id', '=', 'media.id')
            ->where('mediables.mediable_type', 'App\Models\User')
            ->where('mediables.tag', '!=', 'avatar')
            ->orderByDesc('media.created_at')
            ->paginate(20);

        $videos = collect([]);

        foreach ($all as $item) {

            if ($item->tag !== 'avatar' && ($item->aggregate_type === 'video' && $item->compressed)){

                $file = Media::find($item->media_id);

                $file->url = $file->getUrl();
                $file->user = User::find($item->mediable_id);

                if ($file->user && $file->user->isFollowedBy($requester))
                    $file->user->followed = 1;
                else
                    $file->user->followed = 0;

                $thumbnail = File::find($file->id)->loadMedia();

                if ($thumbnail->media->count() > 0){
                    $file->thumbnail = $thumbnail->media[0]->getUrl();
                } else {
                    $file->thumbnail = '';
                }

                $file->comments = Comment::where('media_id', $file->id)->get();

                $file->title = $item->tag;

                $company = \DB::table('mediables')
                    ->where('media_id', $file->id)
                    ->where('mediable_type', 'App\Models\Company')
                    ->where('tag', $file->title)
                    ->first();
                
                if ($company){
                    $company = Company::find($company->mediable_id);
                    $file->company_category = CompanyCategory::find($company->company_category_id)->title;
                    $file->company_subcategory = CompanySubcategory::find($company->company_subcategory_id)->title;
                    $file->region = Region::find($company->region_id)->title;
                }

                $videos->push($file);

            }

        }

        $videos = $videos->sortByDesc('created_at')->values()->all();

        return ['data' => ['info' => $all, 'videos' => $videos]];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = auth()->guard('api')->user();
        $recipient = User::find($id);
        $user->unfollow($recipient);

        $users = $user->getFollowingList();

        return responder()->success($users);
    }

}
