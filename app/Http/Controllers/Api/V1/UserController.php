<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\CreateUser;
use App\Http\Requests\UpdateUser;
use App\Jobs\SendNewUserNotification;
use App\Jobs\SendPushNotification;
use App\Models\Company;
use App\Models\CompanyCategory;
use App\Models\CompanySubcategory;
use App\Models\File;
use App\Models\Region;
use App\Models\User;
use App\Transformers\UserTransformer;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Flugg\Responder\Exceptions\Http\UnauthenticatedException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use libphonenumber\PhoneNumberFormat;

class UserController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUser $request)
    {
        app()->setLocale(request('lang'));

        $user = new User();
        $user->email = $request->get('email');
        $user->password = bcrypt($request->get('password'));
        $user->name = $request->get('name');
        $user->phone = phone($request->get('phone'), $country_code = $request->get('country'), $format = PhoneNumberFormat::E164);
        $user->country = $request->get('country');
        $user->api_token = str_random(60);
        $user->push_id = $request->get('push_id');
        $user->language = request('lang');
        $user->save();

        $now = Carbon::now();
        if ($now->hour < 18){
            $schedule = Carbon::now();
            $schedule->hour = 18;
            $schedule->minute = 0;
            $schedule->second = 0;
        } else {
            $schedule = Carbon::now()->addDay();
            $schedule->hour = 18;
            $schedule->minute = 0;
            $schedule->second = 0;
        }

        if ($user->language === 'en'){
            $text = 'Welcome to Ytravel. From now on travel and earn money. Want to know how? For details click here';
        } else {
            $text = 'ברוכים הבאים ל YTravel. מעכשיו מטיילים ומרוויחים כסף. רוצה לדעת איך? לפרטים לחץ כאן';
        }

        dispatch(new SendNewUserNotification($user));
        dispatch(new SendPushNotification($user, $text, $schedule->format('Y-m-d H:i:s'), ['topic' => 'registration']));

        return responder()->success($user, function ($user) {
            return ['id' => $user->id, 'api_token' => $user->api_token];
        })->respond();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        app()->setLocale(request('lang'));

        $requester = auth()->guard('api')->user();

        $user->loadMedia();
        $user->videos = collect([]);

        foreach ($user->media as $file){

            $file->url = $file->getUrl();

            if ($file->aggregate_type === 'video' && $file->compressed){

                $thumbnail = File::find($file->id)->loadMedia();

                if ($thumbnail->media->count() > 0){
                    $file->thumbnail = $thumbnail->media[0]->getUrl();
                }

                $company = \DB::table('mediables')
                    ->where('media_id', $file->id)
                    ->where('mediable_type', 'App\Models\Company')
                    ->where('tag', $file->pivot->tag)
                    ->first();

                if ($company){
                    $company = Company::find($company->mediable_id);
                    $file->company_category = CompanyCategory::find($company->company_category_id)->title;
                    $file->company_subcategory = CompanySubcategory::find($company->company_subcategory_id)->title;
                    $file->region = Region::find($company->region_id)->title;
                }

                $user->videos->push($file);
            }

        }

        $user->with('coupons');
        $user->coupons->pluck('company');

        if ($requester->id !== $user->id){

            if ($user->isFollowedBy($requester))
                $user->followed = 1;
            else
                $user->followed = 0;

        }

        return responder()->success($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUser $request, User $user)
    {
        app()->setLocale(request('lang'));

        $user->name = $request->get('name');
        $user->phone = phone($request->get('phone'), $country_code = $request->get('country'), $format = PhoneNumberFormat::E164);
        $user->country = $request->get('country');
        $user->save();

        return responder()->success($user);
    }

    public function changePassword(Request $request, User $user)
    {

        app()->setLocale(request('lang'));

        if (!Hash::check($request->get('old_password'), $user->password))
            return responder()->error('invalid_old_password')->respond();

        $user->password = bcrypt($request->get('new_password'));
        $user->save();

        return responder()->success();

    }

    public function changeLanguage(Request $request, User $user)
    {
        $language = $request->get('language');
        app()->setLocale($request->get('language'));

        $user->language = $language;
        $user->save();

        return responder()->success();
    }

}
