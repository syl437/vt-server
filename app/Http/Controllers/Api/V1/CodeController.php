<?php

namespace App\Http\Controllers\Api\V1;

use Aira\Promocodes\Model\Promocode;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCode;
use App\Jobs\SendSMS;
use App\Models\Code;
use App\Models\Manager;
use App\Models\User;
use Illuminate\Http\Request;
use libphonenumber\PhoneNumberFormat;
use Promocodes;


class CodeController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCode $request)
    {
        $manager_id = $request->get('manager_id');
        $quantity = $request->get('quantity');
        $language = $request->get('language');

        try {
            $phone = phone($request->get('phone'), $country_code = $request->get('country'), $format = PhoneNumberFormat::E164);
        } catch (\Exception $e) {
            return responder()->error('invalid_phone')->respond();
        }

        $company = Manager::find($manager_id)->company;
        $text = $company->getTranslation($language)->sms;

        $code = Promocodes::create($manager_id, 1, $quantity);

        if ($language == 'en'){
            $msg = $text . ' For details click here: https://app.y-travel.net/landing?code=' . $code[0]['code'] . '&points=' . $quantity . '&lang=' . $language . '&company=' . $company->id;
        } else {
            $msg = $text . ' לפרטים לחץ כאן: ' . 'https://app.y-travel.net/landing?code=' . $code[0]['code'] . '&points=' . $quantity . '&lang=' . $language . '&company=' . $company->id;

        }

        dispatch(new SendSMS($msg, $phone, $request->get('country')));

        return ["data" => ['phone' => $phone]];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Code $code)
    {
        $code->delete();

        return responder()->success();
    }

    public function validateCode(Request $request)
    {
        app()->setLocale(request('lang'));

        $code = $request->get('code');
        $status = Promocodes::check($code);

        if (!$status)
            return responder()->error('no_code')->respond();

        $data = Code::where('code', $code)
            ->where('rewardable_id', NULL)
            ->where('rewardable_type', NULL)
            ->where('is_used', 0)
            ->first();

        $data->company = Manager::find($data->label)->company;

        return responder()->success($data);
    }

    public function bindCodeToUser(Code $code, User $user)
    {
        $user->applyCode($code->code, function(Promocode $promocode) use ($user) {

            $promocode->rewardable()->associate($user);
            $promocode->save();

        });

        $user->points += $code->reward;
        $user->save();

        return responder()->success($code);
    }

}
