<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateManagerToken;
use App\Http\Requests\CreateUserToken;
use App\Http\Requests\SendPassword;
use App\Jobs\SendNewPasswordEmail;
use App\Models\Manager;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TokenController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateUserToken $request
     * @return \Illuminate\Http\Response
     */
    public function loginUsers(CreateUserToken $request)
    {

        app()->setLocale(request('lang'));

        $credentials = $request->only(['email', 'password']);

        if (!Auth::guard('web')->validate($credentials))
            return responder()->error('invalid_credentials')->respond();

        $user = User::whereEmail($request->get('email'))->first();
        $user->api_token = str_random(60);
        $user->push_id = $request->get('push_id');
        $user->save();

        return responder()->success($user, function ($user) {
            return ['id' => $user->id, 'api_token' => $user->api_token];
        })->respond();
    }

    public function removePushId(User $user)
    {
        $user->push_id = null;
        $user->save();

        return responder()->success();
    }

    public function loginManagers(CreateManagerToken $request)
    {
        $credentials = $request->only(['email', 'password']);

        if (!Auth::guard('admin_web')->validate($credentials))
            return responder()->error('invalid_credentials')->respond();

        $manager = Manager::whereEmail($request->get('email'))->first();

        $manager->api_token = str_random(60);
        $manager->save();

        $cat = $manager->company->company_subcategory->company_category;
        $hotel = $cat->id == 1 ? 1 : 0;

        return responder()->success($manager, function ($manager) use ($hotel) {
            return ['id' => $manager->id, 'api_token' => $manager->api_token, 'company_id' => $manager->company_id, 'is_hotel' => $hotel];
        })->respond();
    }

    public function sendPassword(SendPassword $request)
    {
        app()->setLocale(request('lang'));

        $email = $request->get('email');

        $user = User::whereEmail($email)->first();

        if (!$user)
            return responder()->error('invalid_email')->respond();

        dispatch(new SendNewPasswordEmail($user->email));

        return responder()->success();
    }
}
