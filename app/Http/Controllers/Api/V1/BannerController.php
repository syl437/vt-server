<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Banner;
use App\Http\Controllers\Controller;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return responder()->success(Banner::all());
    }
}
