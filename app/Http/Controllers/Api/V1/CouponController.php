<?php

namespace App\Http\Controllers\Api\V1;

use App\Exceptions\InvalidCouponException;
use App\Http\Requests\CreateCoupon;
use App\Models\Company;
use App\Models\Coupon;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Toin0u\Geotools\Facade\Geotools;


class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        app()->setLocale(request('lang'));

        $user = auth()->guard('api')->user();
        $user->load('coupons');

        // for 1 company

        if ($request->get('company_id') != 0){

            $company = Company::find($request->get('company_id'));

            $users = $company->coupons->pluck('users')->collapse();

            // check if the user already has coupons of this company

            foreach ($users as $coupon_user){

                if ($coupon_user->id === $user->id)
                    $company->bought = 1;

            }

            $company->load('coupons');

            // if the user already has coupons of this company - display him only this coupon

            if ($company->bought){

                $company->coupon = NULL;

                foreach ($user->coupons as $coupon){

                    if ($coupon->pivot->user_id === $user->id){
                        $company->coupon = $coupon;
                    }

                }

                unset($company->coupons);
            }

            return responder()->success([$company]);

        }

        // for all companies

        $lat = $request->get('lat');
        $lng = $request->get('lng');

        $companies = Company::where('approved', 1)->get();

        foreach ($companies as $company){

            $users = $company->coupons->pluck('users')->collapse();

            foreach ($users as $coupon_user){

                if ($coupon_user->id === $user->id)
                    $company->bought = 1;

            }

            $coordA   = Geotools::coordinate([$lat, $lng]);
            $coordB   = Geotools::coordinate([$company->lat, $company->lng]);
            $distance = Geotools::distance()->setFrom($coordA)->setTo($coordB);

            $company->distance = $distance->in('km')->haversine();

            $company->load('coupons');

            if ($company->bought){

                $company->coupon = NULL;

                foreach ($user->coupons as $coupon){

                    if ($coupon->pivot->user_id === $user->id && $coupon->company_id === $company->id){
                        $company->coupon = $coupon;
                    }

                }

                unset($company->coupons);
            }

        }

        $companies = $companies->sortBy('distance')->values()->all();

        return responder()->success($companies);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCoupon $request)
    {
        app()->setLocale(request('lang'));

        $coupon = new Coupon();
        $coupon->price = $request->get('price');
        $coupon->quantity = $request->get('quantity');
        $coupon->company()->associate($request->get('company_id'));
        $coupon->save();

        $coupon->translateOrNew('en')->title = $request->get('title_en');
        $coupon->translateOrNew('he')->title = $request->get('title_he');
        $coupon->translateOrNew('en')->description = $request->get('description_en');
        $coupon->translateOrNew('he')->description = $request->get('description_he');
        $coupon->save();

        return responder()->success($coupon);
    }

    public function getCompanyCoupons(Company $company)
    {
        app()->setLocale(request('lang'));
        return responder()->success($company->coupons());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Coupon $coupon)
    {
        app()->setLocale(request('lang'));
        return responder()->success($coupon);
    }

    public function showAllTranslations(Coupon $coupon)
    {
        $coupon->getTranslationsArray();
        foreach ($coupon->translations as $tr){
            if ($tr->locale === 'en'){
                $coupon->title_en = $tr->title;
                $coupon->description_en = $tr->description;
            }
            if ($tr->locale === 'he'){
                $coupon->title_he = $tr->title;
                $coupon->description_he = $tr->description;
            }
        }
        return responder()->success($coupon);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Coupon $coupon)
    {
        app()->setLocale(request('lang'));
        $coupon->fill($request->all());
        $coupon->save();

        $coupon->translateOrNew('en')->title = $request->get('title_en');
        $coupon->translateOrNew('he')->title = $request->get('title_he');
        $coupon->translateOrNew('en')->description = $request->get('description_en');
        $coupon->translateOrNew('he')->description = $request->get('description_he');
        $coupon->save();

        return responder()->success();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coupon $coupon)
    {
        app()->setLocale(request('lang'));

        $company = Company::find($coupon->company_id);

        $coupon->delete();

        $company->refresh();

        return responder()->success($company->coupons());

    }

    public function bindCouponToUser(Coupon $coupon, User $user)
    {
        app()->setLocale(request('lang'));

        $user->coupons()->attach($coupon, ['expiration_date' => Carbon::today()->addMonth()->format('Y-m-d'), 'used' => 0]);

        $coupon->quantity -= 1;
        $coupon->save();

        $user->points -= $coupon->price;
        $user->save();

        return responder()->success($user);
    }

    public function processQrCode(Request $request) {

        app()->setLocale(request('lang'));

        $code = Crypt::decrypt($request->get('code'));

        $coupon_user = DB::table('coupon_user')
            ->where('coupon_id', $code['coupon_id'])
            ->where('user_id', $code['user_id'])
            ->where('used', 0)
            ->first();

        if (!$coupon_user)
            return responder()->error('invalid_coupon')->respond();

        $exp_date = Carbon::createFromFormat('Y-m-d', $coupon_user->expiration_date);

        if ($exp_date->lt(Carbon::today()))
            return responder()->error('invalid_coupon')->respond();

        DB::table('coupon_user')
            ->where('coupon_id', $code['coupon_id'])
            ->where('user_id', $code['user_id'])
            ->update(['used' => 1, 'updated_at' => Carbon::now()]);

        return responder()->success();

    }
}
