<?php

namespace App\Http\Requests;

use Flugg\Responder\Http\MakesResponses;
use Illuminate\Foundation\Http\FormRequest;

class CreateCompany extends FormRequest
{
    use MakesResponses;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_subcategory_id' => 'required',
            'region_id' => 'required',
            'title_en' => 'required|string',
            'title_he' => 'required|string',
            'phone' => 'string',
            'address' => 'required|string',
            'country' => 'required|string',
            'lat' => 'required',
            'lng' => 'required',
            'email' => 'required|unique:managers',
            'password' => 'required'
        ];
    }
}
