<?php

namespace App\Http\Requests;

use Flugg\Responder\Http\MakesResponses;
use Illuminate\Foundation\Http\FormRequest;

class CreateLead extends FormRequest
{
    use MakesResponses;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required',
            'company_id' => 'required',
            'quantity' => 'required',
            'start' => 'required',
        ];
    }
}
