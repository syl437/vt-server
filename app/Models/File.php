<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Plank\Mediable\Mediable;

class File extends Model
{

    use Mediable;

    protected $table = 'media';

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}
