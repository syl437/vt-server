<?php

namespace App\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;


class CompanyCategory extends Model
{
    use Translatable;

    protected $with = ['company_subcategories'];
    public $translatedAttributes = ['title'];

    public function company_subcategories()
    {
        return $this->hasMany(CompanySubcategory::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough|Company
     */
    public function companies()
    {
        return $this->hasManyThrough(Company::class, CompanySubcategory::class);
    }
}
