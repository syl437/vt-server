<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Plank\Mediable\Mediable;

class Banner extends Model
{
    use Mediable;

    protected $appends = ['image', 'company'];

    protected $fillable = ['type', 'company_id', 'language'];

    protected $hidden = [
        'created_at', 'updated_at', 'media',
    ];

    public function getImageAttribute()
    {
        $banner = $this->getMedia('image');
        if ($banner->count() > 0){
            return $banner[0]->getUrl();
        }
        return '';
    }

    public function getCompanyAttribute()
    {
        $company = Company::find($this->company_id);
        if ($company)
            return $company;

        return 0;
    }

    public function media()
    {
        return $this->morphToMany(config('mediable.model'), 'mediable')
            ->withPivot('tag', 'order', 'mediable_type')
            ->orderBy('order');
    }
}
