<?php

namespace App\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class CompanySubcategory extends Model
{
    use Translatable;

    public $translatedAttributes = ['title'];

    public function companies()
    {
        return $this->belongsToMany(Company::class);
    }

    public function company_category()
    {
        return $this->belongsTo(CompanyCategory::class);
    }

}
