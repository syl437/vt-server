<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['content'];

    protected $appends = ['user'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function media()
    {
        return $this->belongsTo(File::class);
    }

    public function getUserAttribute()
    {
        return User::find($this->user_id);
    }
}
