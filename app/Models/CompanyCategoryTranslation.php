<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyCategoryTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['title'];
}
