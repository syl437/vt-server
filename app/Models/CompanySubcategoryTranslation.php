<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanySubcategoryTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['title'];
}
