<?php

namespace App\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    use Translatable;

    public $translatedAttributes = ['title'];

    public function companies()
    {
        return $this->belongsToMany(Company::class);
    }

}
