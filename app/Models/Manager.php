<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Manager  extends Authenticatable
{
    protected $hidden = [
        'password', 'created_at', 'updated_at'
    ];

    protected $fillable = ['name', 'email', 'password'];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
