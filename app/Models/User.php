<?php

namespace App\Models;

use Aira\Promocodes\Traits\Rewardable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Plank\Mediable\Mediable;
use Skybluesofa\Followers\Traits\Followable;

class User extends Authenticatable
{
    use Notifiable, Rewardable, Mediable, Followable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'country', 'city'
    ];

    protected $appends = [
        'avatar', 'followers'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'created_at', 'updated_at', 'media', 'api_token'
    ];

    /**
     * Relationship for all attached media.
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function media()
    {
        return $this->morphToMany(config('mediable.model'), 'mediable')
            ->withPivot('tag', 'order', 'mediable_type')
            ->orderBy('order');
    }

    public function coupons()
    {
        return $this->belongsToMany(Coupon::class)->withPivot('expiration_date', 'used')->withTimestamps();
    }

    public function getFollowersAttribute()
    {
        return DB::table('followers')->where('recipient_id', $this->id)->count();
    }

    public function getAvatarAttribute()
    {
        $avatar = $this->getMedia('avatar');

        if ($avatar->count() > 0)
            return $avatar[0]->getUrl();

        return '';
    }

    public function leads()
    {
        return $this->hasMany(Lead::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }

    public function rates()
    {
        return $this->hasMany(Rate::class);
    }

}
