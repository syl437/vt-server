<?php

namespace App\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;
use Plank\Mediable\Mediable;

class Coupon extends Model
{
    use Mediable, Translatable;

    protected $fillable = [
        'title', 'price',  'description', 'quantity',
    ];

    public $translatedAttributes = ['title', 'description'];

    protected $appends = ['image', 'qr'];

    protected $hidden = ['created_at', 'updated_at'];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class)->withPivot('expiration_date', 'used');
    }

    public function getImageAttribute()
    {
        $image = $this->getMedia('coupon');

        if ($image->count() > 0)
            return $image[0]->getUrl();

        return '';
    }

    public function getQrAttribute()
    {
        if ($this->pivot){

            return Crypt::encrypt(['coupon_id' => $this->id, 'user_id' => $this->pivot->user_id]);

        }

        return '';
    }

    /**
     * Relationship for all attached media.
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function media()
    {
        return $this->morphToMany(config('mediable.model'), 'mediable')
            ->withPivot('tag', 'order', 'mediable_type')
            ->orderBy('order');
    }

}
