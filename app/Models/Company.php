<?php

namespace App\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Plank\Mediable\Mediable;

class Company extends Model
{
    use Mediable, Translatable;


    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::deleting(function (Company $company) {

            Banner::where('company_id', $company->id)->delete();
//            $media = DB::table('mediables')->where('mediable_type', 'App\Models\Company')->where('mediable_id', $company->id)->get();
//
//            foreach ($media as $file){
//                DB::table('mediables')->where('media_id', $file->media_id)->delete();
//            }

        });
    }


    protected $fillable = [
        'address', 'lat', 'lng', 'website', 'country'
    ];

    public $translatedAttributes = ['title', 'description', 'facebook', 'sms'];

    protected $hidden = [
        'password', 'created_at', 'updated_at', 'media', 
    ];

    protected $appends = [
        'logo', 'main_video', 'company_category_id', 'thumbnail', 'pin', 'rate', 'stars'
    ];

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function company_subcategory()
    {
        return $this->belongsTo(CompanySubcategory::class);
    }

    public function coupons()
    {
        return $this->hasMany(Coupon::class);
    }

    public function managers()
    {
        return $this->hasMany(Manager::class);
    }

    public function getLogoAttribute()
    {
        $logo = $this->getMedia('logo');

        if ($logo->count() > 0)
            return $logo[0]->getUrl();

        return '';
    }

    public function getMainVideoAttribute()
    {
        $main_video = $this->getMedia('main_video');

        if ($main_video->count() > 0)
            return $main_video[0]->getUrl();

        return '';
    }

    public function getThumbnailAttribute()
    {
        $main_video = $this->getMedia('main_video');

        if ($main_video->count() > 0) {

            $file = File::find($main_video[0]->id)->loadMedia();

            if ($file->media->count() > 0){
                return $file->media[0]->getUrl();
            }

        }

        return '';
    }

    public function getPinAttribute()
    {
        $logo = $this->getMedia('logo');

        if ($logo->count() > 0) {

            $file = File::find($logo[0]->id)->loadMedia();

            if ($file->media->count() > 0){
                return $file->media[0]->getUrl();
            }

        }

        return '';
    }

    public function getCompanyCategoryIdAttribute()
    {
        $sub = CompanySubcategory::find($this->company_subcategory_id);
        return $sub->company_category_id;

    }

    public function getLatAttribute($value)
    {
        return floatval($value);

    }

    public function getLngAttribute($value)
    {
        return floatval($value);

    }

    /**
     * Relationship for all attached media.
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function media()
    {
        return $this->morphToMany(config('mediable.model'), 'mediable')
            ->withPivot('tag', 'order', 'mediable_type')
            ->orderBy('order');
    }

    public function leads()
    {
        return $this->hasMany(Lead::class);
    }

    public function rates()
    {
        return $this->hasMany(Rate::class);
    }

    public function getRateAttribute()
    {
        $rates = Rate::where('company_id', $this->id)->get();

        if ($rates->count() > 0)
            return ceil($rates->sum('mark') / $rates->count());

        return 0;
    }


    public function getStarsAttribute()
    {
        $rate = 0;
        $stars = [];
        $rates = Rate::where('company_id', $this->id)->get();

        if ($rates->count() > 0)
            $rate = ceil($rates->sum('mark') / $rates->count());

        for ($i = 0; $i < $rate; $i++){
            $stars[] = true;
        }

        for ($j = 0; $j < (5 - $rate); $j++){
            $stars[] = false;
        }
        return $stars;
    }
}
