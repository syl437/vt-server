<?php

use Illuminate\Routing\Router;

Route::get('/landing', 'Web\V1\LandingController@index');
Route::get('/video', 'Web\V1\LandingController@show');

app('router')->group(['prefix' => 'v1'], function (Router $r) {

    $r->post('login', 'Web\V1\TokenController@login');


    $r->resource('companies', 'Web\V1\CompanyController', ['only' => ['index', 'store', 'show', 'destroy', 'update']]);
    $r->post('files/{file}/status', 'Web\V1\MediaController@changeStatus');
    $r->resource('files', 'Web\V1\MediaController', ['only' => ['index', 'store', 'show', 'destroy', 'update']]);
    $r->resource('files.comments', 'Web\V1\CommentController', ['only' => ['index', 'destroy']]);
    $r->resource('regions', 'Web\V1\RegionController', ['only' => ['index', 'store', 'destroy', 'update']]);
    $r->resource('company_categories', 'Web\V1\CompanyCategoryController', ['only' => ['index', 'store', 'show', 'destroy', 'update']]);
    $r->resource('company_subcategories', 'Web\V1\CompanySubcategoryController', ['only' => ['store', 'destroy', 'update']]);
    $r->resource('faq', 'Web\V1\FaqController', ['only' => ['index', 'store', 'show', 'destroy', 'update']]);
    $r->resource('banners', 'Web\V1\BannerController', ['only' => ['index', 'store', 'show', 'destroy', 'update']]);
    $r->resource('companies.coupons', 'Web\V1\CouponController', ['only' => ['index', 'store', 'show', 'destroy', 'update']]);
    $r->get('companies/{company}/managers/{manager}/credentials', 'Web\V1\ManagerController@resendCredentials');
    $r->get('companies/{company}/purchases', 'Web\V1\CompanyController@getPurchases');
    $r->get('companies/{company}/approve', 'Web\V1\CompanyController@approveCompany');
    $r->get('companies/{company}/suspend', 'Web\V1\CompanyController@suspendCompany');
    $r->get('companies/{company}/managers/{manager}/countries', 'Web\V1\ManagerController@getCountries');
    $r->resource('companies.managers', 'Web\V1\ManagerController', ['only' => ['index', 'store', 'show', 'destroy', 'update']]);
    $r->resource('companies.codes', 'Web\V1\CodeController', ['only' => ['index', 'store']]);
    $r->resource('leads', 'Web\V1\LeadController', ['only' => ['index', 'destroy']]);
    $r->resource('messages', 'Web\V1\MessageController', ['only' => ['index', 'destroy']]);
    $r->resource('points', 'Web\V1\PointController', ['only' => ['index', 'store', 'destroy']]);
    $r->post('users/{user}/points', 'Web\V1\UserController@sendPoints');
    $r->post('users/push', 'Web\V1\UserController@sendPush');
    $r->resource('users', 'Web\V1\UserController', ['only' => ['index', 'destroy']]);
    $r->resource('admins', 'Web\V1\AdminController', ['only' => ['show', 'update']]);

});