<?php

use Illuminate\Routing\Router;

app('router')->group(['prefix' => 'v1'], function (Router $r) {

    $r->post('tokens/managers', 'Api\V1\TokenController@loginManagers');

});

app('router')->group(['middleware' => 'auth:admin_api', 'prefix' => 'v1'], function (Router $r) {

    $r->get('init/countries', 'Api\V1\InitializationController@getCountries');
    $r->resource('init', 'Api\V1\InitializationController', ['only' => ['show']]);

    $r->resource('managers', 'Api\V1\ManagerController', ['only' => ['index', 'show', 'update', 'store', 'destroy']]);

    $r->get('companies/{company}/stats', 'Api\V1\CompanyController@getStatistics');
    $r->resource('companies', 'Api\V1\CompanyController', ['only' => ['show', 'update']]);

    $r->get('qr', 'Api\V1\CouponController@processQrCode');
    $r->get('companies/{company}/admin', 'Api\V1\CompanyController@showForAdmin');
    $r->patch('companies/{company}/updateAdmin', 'Api\V1\CompanyController@updateForAdmin');
    $r->get('companies/{company}/coupons', 'Api\V1\CouponController@getCompanyCoupons');
    $r->get('coupons/{coupon}/allTranslations', 'Api\V1\CouponController@showAllTranslations');
    $r->resource('coupons', 'Api\V1\CouponController', ['only' => ['index', 'update', 'store', 'show', 'destroy']]);

    $r->resource('files', 'Api\V1\MediaController', ['only' => ['update', 'store', 'destroy']]);

    $r->resource('codes', 'Api\V1\CodeController', ['only' => ['store', 'destroy']]);
});
