<?php

use Illuminate\Routing\Router;

app('router')->group(['prefix' => 'v1'], function (Router $r) {

    $r->get('users/{user}/push', 'Api\V1\TokenController@removePushId');
    $r->post('tokens/users', 'Api\V1\TokenController@loginUsers');
    $r->post('tokens/password', 'Api\V1\TokenController@sendPassword');
    $r->get('init/countries', 'Api\V1\InitializationController@getCountries');
    $r->resource('users', 'Api\V1\UserController', ['only' => ['store']]);
    $r->get('makeStandard', 'Api\V1\InitializationController@makeStandard');

});

app('router')->group(['middleware' => 'auth:api', 'prefix' => 'v1'], function (Router $r) {

    // initialization

    $r->resource('init', 'Api\V1\InitializationController', ['only' => ['index']]);

    // users

    $r->get('users/{user}/locale', 'Api\V1\UserController@changeLanguage');
    $r->post('users/{user}/password', 'Api\V1\UserController@changePassword');
    $r->resource('users', 'Api\V1\UserController', ['only' => ['show', 'update']]);

    // companies

    $r->get('companies/reviews', 'Api\V1\CompanyController@getUserReviews');
    $r->post('companies/sorted', 'Api\V1\CompanyController@sortBySubcategoryAndRegion');
    $r->resource('companies', 'Api\V1\CompanyController', ['only' => ['index', 'store', 'show']]);

    // company categories

    $r->resource('company_categories', 'Api\V1\CompanyCategoryController', ['only' => ['index']]);

    // regions

    $r->resource('regions', 'Api\V1\RegionController', ['only' => ['index']]);

    // media

    $r->resource('files', 'Api\V1\MediaController', ['only' => ['index', 'show', 'update', 'store', 'destroy']]);

    // coupons

    $r->get('coupons/{coupon}/users/{user}/bind', 'Api\V1\CouponController@bindCouponToUser');
    $r->resource('coupons', 'Api\V1\CouponController', ['only' => ['index']]);

    // codes

    $r->post('codes/validate', 'Api\V1\CodeController@validateCode');
    $r->get('codes/{code}/users/{user}/bind', 'Api\V1\CodeController@bindCodeToUser');

    // leads

    $r->resource('leads', 'Api\V1\LeadController', ['only' => ['store']]);

    // followers

    $r->resource('followers', 'Api\V1\FollowerController', ['only' => ['index', 'store', 'destroy', 'show']]);

    // comments

    $r->resource('comments', 'Api\V1\CommentController', ['only' => ['index', 'store', 'destroy', 'show']]);

    // rates

    $r->get('users/{user}/companies/{company}/check', 'Api\V1\RateController@check');
    $r->resource('rates', 'Api\V1\RateController', ['only' => ['index', 'store', 'destroy', 'show']]);

    // banners

    $r->resource('banners', 'Api\V1\BannerController', ['only' => ['index']]);

    // messages

    $r->resource('messages', 'Api\V1\MessageController', ['only' => ['store']]);

    // push notifications

    $r->resource('notifications', 'Api\V1\NotificationController', ['only' => ['index']]);

});
