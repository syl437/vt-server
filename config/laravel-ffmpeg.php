<?php

return [
    'default_disk' => 'media',

    'ffmpeg.binaries' => env('FFMPEG_LOCATION'),
    'ffmpeg.threads'  => 12,

    'ffprobe.binaries' => env('FFPROBE_LOCATION'),

    'timeout' => 3600,
];
